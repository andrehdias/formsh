package hujbb.informatica.apac.bean;

import hujbb.informatica.apac.entidades.ArquivosLogCarga;
import hujbb.informatica.apac.util.F;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@ViewScoped
public class ArquivosLogCargaBean implements Serializable {

    private String caminho = FacesContext.getCurrentInstance().getExternalContext().getRealPath("")
                + F.getArqLogCaminho();
    private String nomeArquivoComCaminho;
    private ArquivosLogCarga arquivo = new ArquivosLogCarga();
    private String conteudoLogCarga2;
    private ArquivosLogCarga arquivoLinhaSelecionado;
    private String nomeArquivoLinhaSelecionado;
    private List<ArquivosLogCarga> listaArquivos = new ArrayList();
    private List<String> listLeituraDeArquivos = new ArrayList();

    @PostConstruct
    public void init() {
        try {
            buscarEListaArquivo();
            System.err.println(FacesContext.getCurrentInstance().getExternalContext().getRealPath("")
                    + F.getArqLogCaminho());
        } catch (IOException ex) {
            Logger.getLogger(ArquivosLogCargaBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void lerLogArquivo() {
        conteudoLogCarga2 = "";
        
        if (arquivoLinhaSelecionado != null) {
            nomeArquivoComCaminho = caminho + arquivoLinhaSelecionado.getNomeArquivo() + ".txt";

            List<String> l = F.LendoArquivos(nomeArquivoComCaminho);

            for (String s : l) {
                conteudoLogCarga2 += "\n" + s;
            }

        } else {

            System.out.println("não executou tava nulo o sistema");
        }

    }
    
    public void buscarEListaArquivo() throws IOException {
      
        //função para listar o arquivo de um diretorio e colocar em uma string
        try (Stream<Path> walk = Files.walk(Paths.get(caminho))) {
            List<String> ListaDeArquivoDoDiretorio = walk.filter(Files::isRegularFile)
                    .map(x -> x.toString()).collect(Collectors.toList());

            //pega a lista de arquivo , e remove o caminho e o .txt
            for (String arquivoDiretorio : ListaDeArquivoDoDiretorio) {
                arquivo.setNomeArquivo(arquivoDiretorio.replace(caminho, "").replace(".txt", ""));

                if (!arquivo.getNomeArquivo().equals("gitkeep")) {

                    System.err.println(arquivo.getNomeArquivo());
                    System.err.println(caminho);

                    nomeArquivoComCaminho = caminho + arquivo.getNomeArquivo() + ".txt";
                    listLeituraDeArquivos = F.LendoArquivos(nomeArquivoComCaminho);
                    if (listLeituraDeArquivos != null && listLeituraDeArquivos.size() > 1) {
                        String[] parts = listLeituraDeArquivos.get(0).split(":");
                        arquivo.setUsuarioResponsavel(parts[1]);  // pegha somente o nome da Pessoa da pessoa formatado

                        // Mostrar data formatada
                        String[] parts2 = listLeituraDeArquivos.get(1).split("DATA:");

                        //      String part2 = parts2[1]; // pegha somente o nome da Pessoa da pessoa formatado
                        arquivo.setDataDeCriacao(parts2[1]);
                        listaArquivos.add(arquivo);
                        arquivo = new ArquivosLogCarga();
                    }

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public ArquivosLogCarga getArquivoLinhaSelecionado() {
        return arquivoLinhaSelecionado;
    }

    public void setArquivoLinhaSelecionado(ArquivosLogCarga arquivoLinhaSelecionado) {
        this.arquivoLinhaSelecionado = arquivoLinhaSelecionado;
    }
    
    public String getConteudoLogCarga2() {
        return conteudoLogCarga2;
    }
    
    public void setConteudoLogCarga2(String conteudoLogCarga2) {
        this.conteudoLogCarga2 = conteudoLogCarga2;
    }


    public String getNomeArquivoLinhaSelecionado() {
        return nomeArquivoLinhaSelecionado;
    }

    public void setNomeArquivoLinhaSelecionado(String nomeArquivoLinhaSelecionado) {
        this.nomeArquivoLinhaSelecionado = nomeArquivoLinhaSelecionado;
    }

    public List<ArquivosLogCarga> getListaArquivos() {
        return listaArquivos;
    }

    public void setListaArquivos(List<ArquivosLogCarga> listaArquivos) {
        this.listaArquivos = listaArquivos;
    }
}
