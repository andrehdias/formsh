package hujbb.informatica.apac.bean;

import hujbb.informatica.apac.util.CargaSIGTAP;
import hujbb.informatica.apac.entidades.Usuario;
import hujbb.informatica.apac.util.F;
import hujbb.informatica.apac.util.execao.ErroSistema;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

@ManagedBean
@ViewScoped
public class ImportarBean implements Serializable {

    private int contBarraProTotal;
    private UploadedFile up;
    private String caminho;
    private String caminhoLogCarga;
    private FileWriter logCarga;
    private PrintWriter gravarLogCarga;
    private Usuario usuario;
    private List<String> linhasTxt;
    private String conteudoLogCarga;
    private boolean disable_btProcessarCarga;
    private boolean rendere_progBar;
    private boolean rendere_linkVisualizarLogCarga;

    public ImportarBean() {
        caminho = "";
        caminhoLogCarga = "";
    }

    @PostConstruct
    public void init() {
        disable_btProcessarCarga = true;
        rendere_linkVisualizarLogCarga = false;
    }

    //Função chamada para mostrar na tela a mensagem de lembrete para realizar backup antes de efetuar o processamento da carga 
    public void mensagemBackup() {
        F.mensagem("ATENÇÂO!", "Certifique que há um backup antes de iniciar a atualização da tabela SIGTAP.", FacesMessage.SEVERITY_INFO);
    }

    //Função que permite ao usuário enviar um arquivo para o servidor, sendo este arquivo armazenado no diretório arqTemp presente na pasta do projeto do sistema
    //Nesta função também é criado o arquivo de log do processamento da carga, armazenado do diretório arqLog também presente na pasta do projeto do sistema
    //Grava no arquivo de log um feedback sobre o upload
    public void doUpload(FileUploadEvent e) throws ErroSistema, IOException {
        caminhoLogCarga = FacesContext.getCurrentInstance().getExternalContext().getRealPath("")
                + F.getArqLogCaminho();
        logCarga = new FileWriter(caminhoLogCarga + "logCarga"
                + new SimpleDateFormat("ddMMyyyy").format(Calendar.getInstance().getTime())
                + ".txt", true);
        gravarLogCarga = new PrintWriter(logCarga);
        gravarLogCarga.println("RESPONSÁVEL: "
                + usuario.getNome());
        gravarLogCarga.println("DATA: "
                + new SimpleDateFormat("dd/MM/yyyy, HH:mm").format(Calendar.getInstance().getTime()));
        gravarLogCarga.flush();
        this.up = e.getFile();
        String fileNameUploaded = up.getFileName(); //nome
        long fileSizeUploaded = up.getSize(); //tamanho
        try {
            //pega o caminho relativo
            caminho = FacesContext.getCurrentInstance().getExternalContext().getRealPath("")
                    +F.getArqTempCaminho() + fileNameUploaded;
            //cria uma copia do arquivo no servidor no na pasta padrao arqTemp
            File f = new File(caminho);
            OutputStream os = null;
            InputStream is = null;
            is = up.getInputstream();
            byte[] b = new byte[is.available()];
            os = new FileOutputStream(f);
            while (is.read(b) > 0) {
                os.write(b);
            }
            os.flush();
            os.close();
            is.close();
        } catch (IOException ex) {
            F.mensagem("60", ex.toString(), FacesMessage.SEVERITY_ERROR);
            Logger.getLogger(ImportarBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        disable_btProcessarCarga = false;
        File arqComp = new File(caminho); //cria uma cópia do arquivo upado para pesquisar no caminho indicado se o arquivo existe
        if (!arqComp.exists()) {
            F.mensagem("ERRO AO EFETUAR UPLOAD!", "", FacesMessage.SEVERITY_ERROR);
            gravarLogCarga.println("");
            gravarLogCarga.println("Erro ao efetuar upload!");
            gravarLogCarga.println("");
            gravarLogCarga.println("CARGA NÃO PROCESSADA, PROCESSAMENTO INTERROMPIDO!");
            gravarLogCarga.println("FINALIZADO EM:"
                    + new SimpleDateFormat("dd/MM/yyyy, HH:mm").format(Calendar.getInstance().getTime()));
            gravarLogCarga.println("_________________________________________________________________________________");
            gravarLogCarga.println("");
            gravarLogCarga.flush();
            gravarLogCarga.close();
        } else {
            F.mensagem("UPLOAD EFETUADO COM SUCESSO!", "", FacesMessage.SEVERITY_INFO);
            gravarLogCarga.println("");
            gravarLogCarga.println("Upload efetuado com sucesso!");
            gravarLogCarga.flush();
        }
    }

    //Chama a classe bean que processa a carga, somente após a verificação da existencia dos arquivos necessários para o processamento
    //Grava no arquivo de log da carga
    //Chama função para ler o arquivo de log da carga efetuada
    public void btProcessarCarga() throws ErroSistema, IOException {
        rendere_linkVisualizarLogCarga = true;
        F.intAux = 0;
        if (verificaExistenciaArqTxt()) {
            gravarLogCarga.println("Todos os arquivos necessários estão presentes.");
            gravarLogCarga.flush();
            new CargaSIGTAP(caminho);
        } else {
            F.mensagem("ERRO AO PROCESSAR CARGA!", "Um ou mais arquivos não encontrados.", FacesMessage.SEVERITY_ERROR);
            gravarLogCarga.println("");
            gravarLogCarga.println("CARGA NÃO PROCESSADA, PROCESSAMENTO INTERROMPIDO!");
            gravarLogCarga.println("FINALIZADO EM:"
                    + new SimpleDateFormat("dd/MM/yyyy, HH:mm").format(Calendar.getInstance().getTime()));
            gravarLogCarga.println("_________________________________________________________________________________");
            gravarLogCarga.println("");
            gravarLogCarga.flush();
            gravarLogCarga.close();
        }
        disable_btProcessarCarga = true;
        F.intAux = 0;
        lerLogCarga();
    }

    //Verifica se todos os arquivos txt necessários para a carga existem no arquivo zip, descompactando arquivo por arquivo e armazenando em uma variável temporária
    //Grava no arquivo de log da carga
    public boolean verificaExistenciaArqTxt() {
        String caminho = FacesContext.getCurrentInstance().getExternalContext().getRealPath("")
                + F.getArqTempCaminho();
        List<String> arquivos = new ArrayList<>();
        arquivos.add("tb_grupo.txt");
        arquivos.add("tb_grupo_layout.txt");
        arquivos.add("tb_sub_grupo.txt");
        arquivos.add("tb_sub_grupo_layout.txt");
        arquivos.add("tb_forma_organizacao.txt");
        arquivos.add("tb_forma_organizacao_layout.txt");
        arquivos.add("tb_ocupacao.txt");
        arquivos.add("tb_ocupacao_layout.txt");
        arquivos.add("tb_cid.txt");
        arquivos.add("tb_cid_layout.txt");
        arquivos.add("tb_financiamento.txt");
        arquivos.add("tb_financiamento_layout.txt");
        arquivos.add("tb_modalidade.txt");
        arquivos.add("tb_modalidade_layout.txt");
        arquivos.add("tb_procedimento.txt");
        arquivos.add("tb_procedimento_layout.txt");
        arquivos.add("rl_procedimento_modalidade.txt");
        arquivos.add("rl_procedimento_modalidade_layout.txt");
        arquivos.add("rl_procedimento_ocupacao.txt");
        arquivos.add("rl_procedimento_ocupacao_layout.txt");
        arquivos.add("rl_procedimento_cid.txt");
        arquivos.add("rl_procedimento_cid_layout.txt");
        arquivos.add("tb_registro.txt");
        arquivos.add("tb_registro_layout.txt");
        arquivos.add("rl_procedimento_registro.txt");
        arquivos.add("rl_procedimento_registro_layout.txt");
        arquivos.add("tb_descricao.txt");
        arquivos.add("tb_descricao_layout.txt");
        gravarLogCarga.println("");
        gravarLogCarga.flush();
        for (String arquivo : arquivos) {
            if (F.descompactarUmArquivoNoZip(this.caminho, arquivo) == null || !F.descompactarUmArquivoNoZip(this.caminho, arquivo).exists()) {
                F.setMsgErro(caminho);
                gravarLogCarga.println("Arquivo " + arquivo + " não encontrado.");
                gravarLogCarga.flush();
                return false;
            }
        }
        return true;
    }

    //Lê o conteudo do arquivo de log do processamento da carga e armazena em uma lista de string usada para mostrar na tela
    public void lerLogCarga() {
        List<String> l = F.LendoArquivos(caminhoLogCarga + "logCarga"
                + new SimpleDateFormat("ddMMyyyy").format(Calendar.getInstance().getTime())
                + ".txt");
        for (String s : l) {
            conteudoLogCarga += "\n" + s;
        }
    }

    //Getteres e setteres das variáveis
    public boolean isRendere_progBar() {
        return rendere_progBar;
    }

    public void setRendere_progBar(boolean rendere_progBar) {
        this.rendere_progBar = rendere_progBar;
    }

    public boolean isRendere_linkVisualizarLogCarga() {
        return rendere_linkVisualizarLogCarga;
    }

    public void setRendere_linkVisualizarLogCarga(boolean rendere_linkVisualizarLogCarga) {
        this.rendere_linkVisualizarLogCarga = rendere_linkVisualizarLogCarga;
    }

    public UploadedFile getUp() {
        return up;
    }

    public void setUp(UploadedFile up) {
        this.up = up;
    }

    public String getCaminho() {
        return caminho;
    }

    public void setCaminho(String caminho) {
        this.caminho = caminho;
    }

    public int getContBarraProTotal() {
        contBarraProTotal = F.intAux;
        return contBarraProTotal;
    }

    public void setContBarraProTotal(int contBarraProTotal) {
        this.contBarraProTotal = contBarraProTotal;
    }

    public boolean isDisable_btProcessarCarga() {
        return disable_btProcessarCarga;
    }

    public void setDisable_btProcessarCarga(boolean disable_btProcessarCarga) {
        this.disable_btProcessarCarga = disable_btProcessarCarga;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<String> getLinhasTxt() {
        return linhasTxt;
    }

    public void setLinhasTxt(List<String> linhasTxt) {
        this.linhasTxt = linhasTxt;
    }

    public String getConteudoLogCarga() {
        return conteudoLogCarga;
    }

    public void setConteudoLogCarga(String conteudoLogCarga) {
        this.conteudoLogCarga = conteudoLogCarga;
    }

    public String getCaminhoLogCarga() {
        return caminhoLogCarga;
    }

    public void setCaminhoLogCarga(String caminhoLogCarga) {
        this.caminhoLogCarga = caminhoLogCarga;
    }

}
