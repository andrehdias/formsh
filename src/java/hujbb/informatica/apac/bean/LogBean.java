package hujbb.informatica.apac.bean;

import hujbb.informatica.apac.dao.AuditoriaDAO;
import hujbb.informatica.apac.dao.SolicitanteDAO;
import hujbb.informatica.apac.entidades.Auditoria;
import hujbb.informatica.apac.entidades.Entidade;
import hujbb.informatica.apac.entidades.Log_procedimento;
import hujbb.informatica.apac.entidades.Solicitante;
import hujbb.informatica.apac.entidades.Usuario;
import hujbb.informatica.apac.util.F;
import hujbb.informatica.apac.util.FabricaDeConexoes;
import hujbb.informatica.apac.util.execao.ErroSistema;
import java.util.ArrayList;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

@ManagedBean
@ViewScoped
public class LogBean extends CrudBean<Auditoria, AuditoriaDAO> {

    private AuditoriaDAO dao;

    private String usuarioLogin;
    private String acaoId;
    private String objetoId;
    private boolean selectBooleanCheckBox_periodo;
    private Date dtIni;
    private Date dtFim;

    //itens
    private ArrayList<SelectItem> usuario_item;
    private ArrayList<SelectItem> acao_item;
    private ArrayList<SelectItem> objeto_item;

    ///controle de tela
    private int qtdRegistro;

    @PostConstruct
    public void init() {
        qtdRegistro = 20;
        buscar(" 1=1  ORDER BY id_auditoria DESC LIMIT " + qtdRegistro);
        selectBooleanCheckBox_periodo = true;
        dtIni = new Date();
        dtFim = new Date();
        usuarioLogin = "";
        acaoId = "";
        objetoId = "";

        //itens
        try {
            setUsuario_item(Usuario.item(""));
            setAcao_item(Log_procedimento.item(""));
            setObjeto_item(Entidade.item(""));
            maisInfo();//mais info fecha a conexao
        } catch (ErroSistema ex) {
        }
    }

    public void buscarLogs() throws ErroSistema {
        String condicao = "(auditoria.id_auditoria <> -1) ";
        if (!selectBooleanCheckBox_periodo) {

            if (dtIni.equals(dtFim)) {
                condicao += " AND (auditoria.data = '" + F.dataStringBanco(dtIni) + "') ";
            } else {
                condicao += " AND (auditoria.data BETWEEN '" + F.dataStringBanco(dtIni) + "' AND '" + F.dataStringBanco(dtFim) + "') ";
            }
        }
        //verifica se algum usuario foi selecionado
        if (!usuarioLogin.isEmpty() && !usuarioLogin.equals("0")) {
            condicao += " AND (usuario.login = '" + usuarioLogin + "')";
        }
        //Verifica se uma acao foi selecionada
        if (!acaoId.isEmpty() && !acaoId.equals("0")) {
            condicao += " AND (log_procedimento.id = '" + acaoId + "')";
        }
        //Verifica se um objeto foi selecionado
        if (!objetoId.isEmpty() && !objetoId.equals("0")) {
            condicao += " AND (entidade.id = '" + objetoId + "')";
        }
        condicao += " ORDER BY id_auditoria DESC LIMIT " + qtdRegistro;
        buscar(condicao);
        maisInfo();//mais info fecha a conexao
    }

    public void maisInfo() throws ErroSistema {
        if (!getEntidades().isEmpty()) {
            for (int i = 0; i < getEntidades().size(); i++) {
                
                    switch (getEntidades().get(i).getTipo_entidade().getId()) {
                        case 4: {//categoria

                            break;
                        }
                        case 5: {//cesta basica

                            break;
                        }
                        case 7: {//check list

                            break;
                        }
                        case 9: {//cliente

                            break;
                        }
                        case 11: {//divida

                            break;
                        }
                        case 12: {//solicitante
                           // System.out.println(getEntidades().get(i).getId_entidade_alvo());
                            Solicitante s = new SolicitanteDAO().buscaId(getEntidades().get(i).getId_entidade_alvo() + "");
                            
                            if (s != null) {
                                getEntidades().get(i).setProcedimento_realizado(
                                        "***"+getEntidades().get(i).getProcedimento_realizado()
                                        + " USUÁRIO: " + s.getNome()
                                );
                                
                            }else{
                                getEntidades().get(i).setProcedimento_realizado(
                                        "***"+getEntidades().get(i).getProcedimento_realizado()
                                        + " USUÁRIO -  FALHA NO CADASTRO!" 
                                );

                                break;
                            }
                            break;
                        }
                        case 16: {//Marca

                            break;
                        }
                        case 17: {//Pagamento

                            break;
                        }

                        case 20: {//Produto

                            break;
                        }
                        case 30: {//Venda

                            break;
                        }
                        case 32: {//Vendedor
                            /* Vendedor c = new VendedorDAO().buscaId(getEntidades().get(i).getId_entidade_alvo() + "");
                            if (c != null) {
                                getEntidades().get(i).setProcedimento_realizado(
                                        getEntidades().get(i).getProcedimento_realizado()
                                        + " - Nome:" + c.getPessoa().getNome()
                                );
                            }*/
                            break;
                        }
                        default: {
                            
                            break;
                        }
                    }
                
            }
        }
        FabricaDeConexoes.fecharConecxao();
    }

    public ArrayList<SelectItem> getObjeto_item() {
        return objeto_item;
    }

    public void setObjeto_item(ArrayList<SelectItem> objeto_item) {
        this.objeto_item = objeto_item;
    }

    public String getObjetoId() {
        return objetoId;
    }

    public void setObjetoId(String objetoId) {
        this.objetoId = objetoId;
    }

    public ArrayList<SelectItem> getAcao_item() {
        return acao_item;
    }

    public void setAcao_item(ArrayList<SelectItem> acao_item) {
        this.acao_item = acao_item;
    }

    public String getAcaoId() {
        return acaoId;
    }

    public void setAcaoId(String acaoId) {
        this.acaoId = acaoId;
    }

    public ArrayList<SelectItem> getUsuario_item() {
        return usuario_item;
    }

    public void setUsuario_item(ArrayList<SelectItem> usuario_item) {
        this.usuario_item = usuario_item;
    }

    public String getUsuarioLogin() {
        return usuarioLogin;
    }

    public void setUsuarioLogin(String usuarioLogin) {
        this.usuarioLogin = usuarioLogin;
    }

    public Date getDtIni() {
        return dtIni;
    }

    public void setDtIni(Date dtIni) {
        selectBooleanCheckBox_periodo = false;
        if (dtIni.after(dtFim)) {
            dtFim = dtIni;
        }
        this.dtIni = dtIni;

    }

    public Date getDtFim() {
        return dtFim;
    }

    public void setDtFim(Date dtFim) {
        selectBooleanCheckBox_periodo = false;
        if (dtFim.before(dtIni)) {
            dtIni = dtFim;
        }
        this.dtFim = dtFim;
    }

    public boolean isSelectBooleanCheckBox_periodo() {
        return selectBooleanCheckBox_periodo;
    }

    public void setSelectBooleanCheckBox_periodo(boolean selectBooleanCheckBox_periodo) {
        this.selectBooleanCheckBox_periodo = selectBooleanCheckBox_periodo;
    }

    public int getQtdRegistro() {
        return qtdRegistro;
    }

    public void setQtdRegistro(int qtdRegistro) {
        this.qtdRegistro = qtdRegistro;
    }

    @Override
    public AuditoriaDAO getDao() {
        if (dao == null) {
            dao = new AuditoriaDAO();
        }
        return dao;
    }

    @Override
    public Auditoria criarNovaEntidade() {
        return new Auditoria();
    }

}
