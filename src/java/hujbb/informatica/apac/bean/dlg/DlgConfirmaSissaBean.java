/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hujbb.informatica.apac.bean.dlg;

import hujbb.informatica.apac.dao.tb_sissaDAO;
import hujbb.informatica.apac.entidades.Formulario;
import hujbb.informatica.apac.entidades.Usuario;
import hujbb.informatica.apac.entidades.imprimir.Pdf_relatorioSissa;
import hujbb.informatica.apac.entidades.auxiliares.tb_sissa;
import hujbb.informatica.apac.util.ContextoUtil;
import hujbb.informatica.apac.util.F;
import hujbb.informatica.apac.util.execao.ErroSistema;
import hujbb.informatica.apac.util.relatorio.Relatorios;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.swing.ImageIcon;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author santana.andre
 */
@ManagedBean
@ViewScoped
public class DlgConfirmaSissaBean implements Serializable{
    private List<tb_sissa> listaTb_sissas;
    public static List<tb_sissa> listaStatica;
    private List<tb_sissa> listaAux;//tudo
    private List <tb_sissa> listaAuxiliar = new ArrayList<>();
    
//    @PostConstruct
//    public void init() {
//        listaAux = new ArrayList<>();
//    }

    public static Date dtIni;
    public static Date dtFim;
    public static String filtroSituacao;
    
    private Date dtIniAux;
    private Date dtFimAux;
    private String filtroSituacaoAux;
    
    private boolean rendereBtGerarXML;
    private Date dataEHoraXml;
    private String caminhoComTitulo;
    private String titulo;
    private StreamedContent file;
    private StreamedContent fileRelatorio;
    private FileInputStream stream;
    private Writer fileWriter;
    private ByteArrayOutputStream baos;
    private HttpServletResponse response;
    private FacesContext context;
    private boolean btXml=false;
    private Integer usuarioLogado;
    private boolean arqGerado;
    
    
    @PostConstruct
    public void init() {
//    dtIniAux=(Date) ContextoUtil.getParamFromSession("dataIniXml");
//    dtFimAux=(Date) ContextoUtil.getParamFromSession("dataFimXml");
//    filtroSituacaoAux= (String) ContextoUtil.getParamFromSession("filtroSituacaoXml");
    }
    
    public void setarListaAux() throws ErroSistema {

        listaAux = listaStatica;
//        int cont = F.retornaSqlUltimoId("SELECT MAX(id_sissa) as cod FROM tb_sissa");
//        for (int j = 0; j < listaAux.size(); j++) {
//            listaAux.get(j).setId_sissa(cont);
//            cont++;
//        }
        listaTb_sissas = listaStatica;
        listaStatica = null;
        arqGerado=false;
    }
    
     public void setarDtIniAux() {

        dtIniAux=dtIni;
        dtIni = null;
    }
     
      public void setarDtFimAux() {

        dtFimAux = dtFim;
        dtFim = null;
    }
      
      public void setarFiltroSituacaoAux() {

        filtroSituacaoAux = filtroSituacao;
        filtroSituacao = null;
    }

    public static List<tb_sissa> getListaStatica() {
        return listaStatica;
    }

    public static void setListaStatica(List<tb_sissa> listaStatica) {
        DlgConfirmaSissaBean.listaStatica = listaStatica;
    }

    public List<tb_sissa> getListaTb_sissas() {
        return listaTb_sissas;
    }

    public void setListaTb_sissas(List<tb_sissa> listaTb_sissas) {
        this.listaTb_sissas = listaTb_sissas;
    }

    public List<tb_sissa> getListaAux() {
        return listaAux;
    }

    public void setListaAux(List<tb_sissa> listaAux) {
        this.listaAux = listaAux;
    }

    public static Date getDtIni() {
        return dtIni;
    }

    public static void setDtIni(Date dtIni) {
        DlgConfirmaSissaBean.dtIni = dtIni;
    }

    public static Date getDtFim() {
        return dtFim;
    }

    public static void setDtFim(Date dtFim) {
        DlgConfirmaSissaBean.dtFim = dtFim;
    }

    public boolean isRendereBtGerarXML() {
        return rendereBtGerarXML;
    }

    public void setRendereBtGerarXML(boolean rendereBtGerarXML) {
        this.rendereBtGerarXML = rendereBtGerarXML;
    }

    public static String getFiltroSituacao() {
        return filtroSituacao;
    }

    public static void setFiltroSituacao(String filtroSituacao) {
        DlgConfirmaSissaBean.filtroSituacao = filtroSituacao;
    }

    public Date getDataEHoraXml() {
        return dataEHoraXml;
    }

    public void setDataEHoraXml(Date dataEHoraXml) {
        this.dataEHoraXml = dataEHoraXml;
    }

    public String getCaminhoComTitulo() {
        return caminhoComTitulo;
    }

    public void setCaminhoComTitulo(String caminhoComTitulo) {
        this.caminhoComTitulo = caminhoComTitulo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public FileInputStream getStream() {
        return stream;
    }

    public void setStream(FileInputStream stream) {
        this.stream = stream;
    }

    public Writer getFileWriter() {
        return fileWriter;
    }

    public void setFileWriter(Writer fileWriter) {
        this.fileWriter = fileWriter;
    }

    public Date getDtIniAux() {
        return dtIniAux;
    }

    public void setDtIniAux(Date dtIniAux) {
        this.dtIniAux = dtIniAux;
    }

    public Date getDtFimAux() {
        return dtFimAux;
    }

    public void setDtFimAux(Date dtFimAux) {
        this.dtFimAux = dtFimAux;
    }

    public String getFiltroSituacaoAux() {
        return filtroSituacaoAux;
    }

    public void setFiltroSituacaoAux(String filtroSituacaoAux) {
        this.filtroSituacaoAux = filtroSituacaoAux;
    }

    public boolean isBtXml() {
        return btXml;
    }

    public void setBtXml(boolean btXml) {
        this.btXml = btXml;
    }

    public Integer getUsuarioLogado() {
        return usuarioLogado;
    }

    public void setUsuarioLogado(Integer usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }
    
    

//    public StreamedContent getFileRelatorio() throws ErroSistema {
//        if (!getListaAux().isEmpty()) {//se a lista de formulário tiver dados
//            //cria uma lista de objetos para gerar a  pdf
//            List<Object> l = new ArrayList<>();
//            Pdf_relatorioSissa pdf_relatorioSissa;
//            int i = F.retornaSqlUltimoId("SELECT MAX(id_sissa) as cod FROM tb_sissa");
//            for (tb_sissa f : getListaAux()) {
//                pdf_relatorioSissa = new Pdf_relatorioSissa(//escreve o relatório de fomrulários emitidos
//                        ++i + "",//numero de ordem
//                        f.getPaciente().getNome(),//Nome do paciente
//                        f.getPaciente().getCns() + "",//Número do cartão SUS
//                        f.getP1().getCodigo(),//Código do procediemnto principal
//                        F.dataString(dtIniAux) + " à " + F.dataString(dtFimAux),//data inicio e fim do período da busca
//                        f.getPaciente().getNum_prontuario(),//Número do prontuário do paciente
//                        f.getPaciente().getMunicipio(),//Número do código de municipio da resindência do paciente
//                        f.getProc_justificativa().getCid_principal().getCid(),//código do cid que foi diagnostico
//                        new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime()),
//                        f.getEstabelecimento_de_saude_solicitante().getCnes(),//código do estabelecimento de saúde
//                        f.getEstabelecimento_de_saude_solicitante().getNome(),//nome do estabelecimento de saude
//                        Integer.toString(getListaAux().size())
//                );
//                l.add(pdf_relatorioSissa);
//            }
//            Relatorios r = new Relatorios("/hujbb/informatica/apac/util/relatorio/apacSissa.jpg", "/hujbb/informatica/apac/util/relatorio/rodapeSissa.jpg");
//
//            fileRelatorio = r.gerarPDFXML(l, "apacEmitidasSissa",new SimpleDateFormat("MM-yyyy").format(dtIniAux));
//        } else {
//            F.mensagem("Sem dados para imprimir!", "", FacesMessage.SEVERITY_INFO);
//        }
//        return fileRelatorio;
//    }
//
//    public void setFileRelatorio(StreamedContent fileRelatorio) {
//        this.fileRelatorio = fileRelatorio;
//    }
    
    
    
    public String mascaraCodProdedimento(String codProced) {//criar mascara da string procedimento principal
        String retur = "";
        for (int i = 0; i < codProced.length(); i++) {
            retur += codProced.charAt(i);
            if (i == 1 || i == 3 || i == 5) {
                retur += ".";
            }
            if (i == 8) {
                retur += "-";
            }
        }
        return retur;
    }
    
        
        
        
        public void btImprimir() throws ErroSistema {
            if (!getListaAux().isEmpty()) {//se a lista de formulário tiver dados
            //cria uma lista de objetos para gerar a  pdf
            List<Object> l = new ArrayList<>();
            Pdf_relatorioSissa pdf_relatorioSissa;
            int i = F.retornaSqlUltimoId("SELECT MAX(id_sissa) as cod FROM tb_sissa");
            for (tb_sissa f : getListaAux()) {
                pdf_relatorioSissa = new Pdf_relatorioSissa(//escreve o relatório de fomrulários emitidos
                        ++i + "",//numero de ordem
                        f.getPaciente().getNome(),//Nome do paciente
                        f.getPaciente().getCns() + "",//Número do cartão SUS
                        f.getP1().getCodigo(),//Código do procediemnto principal
                        F.dataString(dtIniAux) + " à " + F.dataString(dtFimAux),//data inicio e fim do período da busca
                        f.getPaciente().getNum_prontuario(),//Número do prontuário do paciente
                        f.getPaciente().getMunicipio(),//Número do código de municipio da resindência do paciente
                        f.getProc_justificativa().getCid_principal().getCid(),//código do cid que foi diagnostico
                        new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime()),
                        f.getEstabelecimento_de_saude_solicitante().getCnes(),//código do estabelecimento de saúde
                        f.getEstabelecimento_de_saude_solicitante().getNome(),//nome do estabelecimento de saude
                        Integer.toString(getListaAux().size())
                );
                l.add(pdf_relatorioSissa);
            }
            Relatorios r = new Relatorios("/hujbb/informatica/apac/util/relatorio/apacSissa.jpg", "/hujbb/informatica/apac/util/relatorio/rodapeSissa.jpg");
            r.gerarPDFXML(l, "apacEmitidasSissa",new SimpleDateFormat("MM-yyyy").format(dtIniAux));
        } else {
            F.mensagem("Sem dados para imprimir!", "", FacesMessage.SEVERITY_INFO);
        }
//        PrimeFaces.current().executeScript("funcao1();");
    }
        
        public String racaCorSissa(Formulario f) {//retorna o código da cor do paciente de acordo com que  o sissa escreve
        switch (f.getPaciente().getCor()) {
            case "A":
                return "04";
            case "B":
                return "01";
            case "I":
                return "05";
            case "M":
                return "03";
            case "P":
                return "02";
            default:
                return "";
        }
    }
        
    public String mascaraProntuario(String prontuario) { // mascara para exibição do prontuário na tela
        String retur = "";

        for (int i = 0; i < prontuario.length(); i++) {
            if (i == 6) {
                retur += "-";
            }
            retur += prontuario.charAt(i);
        }
        return retur;
    }

    public StreamedContent getFile() throws IOException, XMLStreamException, ErroSistema {
        
        btXml=true;
        
        if (arqGerado == false) {

            for (tb_sissa f : getListaAux()) {
                f.getStatus().setId_status(4);
                String sql = "UPDATE formulario SET status_id_status= " + f.getStatus().getId_status().toString()
                        + " WHERE id_formulario= " + f.getId_formulario().toString();
                try {
                    F.executSql(sql);
                    dataEHoraXml = new Date();
                    titulo = "AU" + "2332981" + new SimpleDateFormat("ddMMyyyyHHmm").format(dataEHoraXml);
                    String caminho = FacesContext.getCurrentInstance().getExternalContext().getRealPath("")
                            + F.getArqTempCaminho();
                    caminhoComTitulo = caminho + titulo + ".xml";
                    f.setDth_criacao(dataEHoraXml);
                    f.setCompetencia(new SimpleDateFormat("MM/yyyy").format(dtIniAux));
                    f.setArquivo(titulo + ".xml");
                    f.setSeqPaciente(f.getPaciente().getCodigoAghu());
                    f.setUsuario(getUsuarioLogado());
                    listaAuxiliar.add(new tb_sissaDAO().salvar(f));
                    arqGerado=true;

                } catch (ErroSistema ex) {
                    F.setMsgErro("exportarSissa:limpaArquivo:" + ex.toString());
                }

            }
        }
       fazOXml(listaAuxiliar); 
       return file; 
    }

    public void limpaArquivo() throws ErroSistema {
        try {
            fileWriter.close();
            stream.close();
            File file = new File(caminhoComTitulo);
            file.delete();
            

        } catch (IOException ex) {
            System.out.println(ex.toString());
        }

    }
    
    public void fazOXml(List <tb_sissa> formulariosSelecionados) throws IOException, XMLStreamException {
        try {
            
//            String titulo = "AU" + "2332981" + new SimpleDateFormat("ddMMyyyyHHmm").format(Calendar.getInstance().getTime());
//        String filePath = "C:\\Documentos\\" + titulo + ".xml";

            fileWriter = new FileWriter(caminhoComTitulo);

            XMLOutputFactory outputFactory = XMLOutputFactory.newFactory();
            XMLStreamWriter xmlStreamWriter = outputFactory.createXMLStreamWriter(fileWriter);

            xmlStreamWriter.writeStartDocument();
            xmlStreamWriter.writeStartElement("DATAPACKET");
            xmlStreamWriter.writeAttribute("Version", "2.0");

            xmlStreamWriter.writeStartElement("METADATA");

            xmlStreamWriter.writeStartElement("FIELDS");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "SO_COD");
            xmlStreamWriter.writeAttribute("fieldtype", "r8");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "SO_CD_COD");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "4");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "SO_DATA_LAUDO");
            xmlStreamWriter.writeAttribute("fieldtype", "SQLdateTime");
            xmlStreamWriter.writeAttribute("SUBTYPE", "Formatted");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "SO_TIPO_SOL");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "1");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "SO_CI_COD");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "2");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "SO_CL_COD");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "2");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "SO_PC_COD_SOL");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "10");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "SO_ES_CNES");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "7");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "SO_SL_CPF");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "11");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "SO_DATA_SOL");
            xmlStreamWriter.writeAttribute("fieldtype", "SQLdateTime");
            xmlStreamWriter.writeAttribute("SUBTYPE", "Formatted");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "SO_QTDE");
            xmlStreamWriter.writeAttribute("fieldtype", "i4");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "SO_LAUDO");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "15");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "SO_LDO");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "15");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "SO_MOTIVO");
            xmlStreamWriter.writeAttribute("fieldtype", "bin.hex");
            xmlStreamWriter.writeAttribute("SUBTYPE", "Binary");
            xmlStreamWriter.writeAttribute("WIDTH", "10000");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "PA_CPF");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "11");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "PA_CNS");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "15");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "PA_NOME");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "60");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "PA_SEXO");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "1");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "PA_DATA_NASC");
            xmlStreamWriter.writeAttribute("fieldtype", "SQLdateTime");
            xmlStreamWriter.writeAttribute("SUBTYPE", "Formatted");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "PA_NOME_MAE");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "60");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "PA_LOGR");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "40");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "PA_NUME_END");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "11");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "PA_COMP");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "25");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "PA_BAIR");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "30");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "PA_RACA_COR");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "2");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "PA_ETNIA");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "4");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "PA_CP_CEP");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "8");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "PA_MU_COD");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "7");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "PA_NOME_RESPONSAVEL");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "60");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "SL_CNS");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "15");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "SL_NOME");
            xmlStreamWriter.writeAttribute("fieldtype", "string");
            xmlStreamWriter.writeAttribute("WIDTH", "60");

            xmlStreamWriter.writeEmptyElement("FIELD");
            xmlStreamWriter.writeAttribute("attrname", "SO_PA_ID");
            xmlStreamWriter.writeAttribute("fieldtype", "i4");

            xmlStreamWriter.writeEndElement();

            String contadorString = "1";//contador string parans
            for (int i = 0; i < formulariosSelecionados.size(); i++) {

                if (contadorString.equals("1")) {
                    contadorString = contadorString + " 0 4";
                } else {
                    contadorString = contadorString + " " + Integer.toString(i + 1) + " 0 4";
                }

            }
            xmlStreamWriter.writeEmptyElement("PARAMS");//Inicio params
            xmlStreamWriter.writeAttribute("CHANGE_LOG", contadorString);
            xmlStreamWriter.writeEndElement();//end params

            xmlStreamWriter.writeStartElement("ROWDATA");//inicio metadata

            int cont = 1;
            for (tb_sissa formToXml : formulariosSelecionados) {

                xmlStreamWriter.writeEmptyElement("ROW");
                xmlStreamWriter.writeAttribute("RowState", "4");
                xmlStreamWriter.writeAttribute("SO_COD", ""+formToXml.getId_sissa());
                xmlStreamWriter.writeAttribute("SO_CD_COD", formToXml.getProc_justificativa().getCid_principal().getCid());
                xmlStreamWriter.writeAttribute("SO_DATA_LAUDO", new SimpleDateFormat("yyyyMMdd").format(formToXml.getPag2().getData_64()));
                xmlStreamWriter.writeAttribute("SO_TIPO_SOL", "A");
                xmlStreamWriter.writeAttribute("SO_CI_COD", "01");
                xmlStreamWriter.writeAttribute("SO_CL_COD", "");
                xmlStreamWriter.writeAttribute("SO_PC_COD_SOL", formToXml.getP1().getCodigo());
                xmlStreamWriter.writeAttribute("SO_ES_CNES", formToXml.getEstabelecimento_de_saude_solicitante().getCnes());
                xmlStreamWriter.writeAttribute("SO_SL_CPF", formToXml.getSolicitante().getCpf());
                xmlStreamWriter.writeAttribute("SO_DATA_SOL", new SimpleDateFormat("yyyyMMdd").format(formToXml.getData()));
                xmlStreamWriter.writeAttribute("SO_QTDE", "1");
                xmlStreamWriter.writeAttribute("SO_LAUDO", mascaraProntuario(formToXml.getPaciente().getNum_prontuario()));
                xmlStreamWriter.writeAttribute("SO_LDO", formToXml.getCodLaudo());
                xmlStreamWriter.writeAttribute("SO_MOTIVO", Base64.getEncoder().encodeToString(formToXml.getProc_justificativa().getObservacoes().getBytes()));
                xmlStreamWriter.writeAttribute("PA_CPF", (formToXml.getPaciente().getCpf() == 0) ? "" : ""+formToXml.getPaciente().getCpf());
                xmlStreamWriter.writeAttribute("PA_CNS", formToXml.getPaciente().getCns());
                xmlStreamWriter.writeAttribute("PA_NOME", formToXml.getPaciente().getNome());
                xmlStreamWriter.writeAttribute("PA_SEXO", (formToXml.getPaciente().getSexo().equals("M")) ? ("1") : ("3"));
                xmlStreamWriter.writeAttribute("PA_DATA_NASC", new SimpleDateFormat("yyyyMMdd").format(formToXml.getPaciente().getData_nascimento()));
                xmlStreamWriter.writeAttribute("PA_NOME_MAE", formToXml.getPaciente().getNome_mae());
                xmlStreamWriter.writeAttribute("PA_LOGR", formToXml.getPaciente().getLogradouro());
                xmlStreamWriter.writeAttribute("PA_NUME_END", formToXml.getPaciente().getNum_residencia());
                xmlStreamWriter.writeAttribute("PA_COMP", "");
                xmlStreamWriter.writeAttribute("PA_BAIR", formToXml.getPaciente().getBairro());
                xmlStreamWriter.writeAttribute("PA_RACA_COR", racaCorSissa(formToXml));//
                xmlStreamWriter.writeAttribute("PA_ETNIA", (formToXml.getPaciente().getEtn_id() == 0) ? "0000" : Integer.toString(formToXml.getPaciente().getEtn_id()));//
                xmlStreamWriter.writeAttribute("PA_CP_CEP", formToXml.getPaciente().getCep());
                xmlStreamWriter.writeAttribute("PA_MU_COD", formToXml.getPaciente().getCod_ibge_municipio());
                xmlStreamWriter.writeAttribute("PA_NOME_RESPONSAVEL", formToXml.getPaciente().getNome_responsavel());
                xmlStreamWriter.writeAttribute("SL_CNS", formToXml.getSolicitante().getCns());
                xmlStreamWriter.writeAttribute("SL_NOME", formToXml.getSolicitante().getNome());
                xmlStreamWriter.writeAttribute("SO_PA_ID", formToXml.getSeqPaciente());
                cont = cont + 1;

            }
            xmlStreamWriter.writeEndDocument();

            //Escrever conteúdo no arquivo XML e fechar xmlStreamWriter.
            xmlStreamWriter.flush();
            xmlStreamWriter.close();
            xmlStreamWriter = null;

            stream = new FileInputStream(caminhoComTitulo);
            file = new DefaultStreamedContent(stream, "application/xml", titulo + ".xml");
//            System.out.println("XML file created successfully.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void btSair() throws ErroSistema{
        if(listaAux.size()>0 && btXml)
        {
            F.fecharDlg(listaAux);
            
        }
        else{
            
            F.fecharDlg("");
        }
       
    }

    
}
