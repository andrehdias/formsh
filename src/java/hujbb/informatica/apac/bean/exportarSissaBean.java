package hujbb.informatica.apac.bean;

import hujbb.informatica.apac.bean.dlg.DlgConfirmaSissaBean;
import hujbb.informatica.apac.dao.Tb_registro_has_procedimento_susDAO;
import hujbb.informatica.apac.entidades.Formulario;
import hujbb.informatica.apac.entidades.Procedimento_sus;
import hujbb.informatica.apac.entidades.Status;
import hujbb.informatica.apac.entidades.Tb_registro_has_procedimento_sus;
import hujbb.informatica.apac.entidades.imprimir.Pdf_relatorioSissa;
import hujbb.informatica.apac.entidades.auxiliares.tb_sissa;
import hujbb.informatica.apac.util.ContextoUtil;
import hujbb.informatica.apac.util.F;
import hujbb.informatica.apac.util.execao.ErroSistema;
import hujbb.informatica.apac.util.relatorio.Relatorios;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.StreamedContent;

@ManagedBean
@ViewScoped
public class exportarSissaBean extends FormularioBean {

//controle tela
    private boolean selectBooleanCheckBox_periodo;
    private boolean rendereBtGerarXML;
    private boolean btEnviarDere;
    private boolean checkBoxMarcado;
    private boolean checkBoxNaoMarcado;
    private boolean rendereCheckBt;
    private ArrayList<SelectItem> status_item;
    private Date dtIni;
    private Date dtFim;
    private String tituloTabela;
    private String filtroSituacao;

    private List<tb_sissa> formsSelecionados;
    private List<tb_sissa> forms;
//    private List<Formulario> formsSelecionadosAux;
//    private List<Formulario> forms;
    private List<Formulario> codigosLaudos;

    @PostConstruct
    public void init() {

        selectBooleanCheckBox_periodo = true;
        rendereBtGerarXML = false;
        dtIni = new Date();
        dtFim = new Date();
        filtroSituacao = "3";
        try {
            status_item = Status.item("id_status = 3");//Adicionar Status emitido ao filtro situação
            status_item.add(new SelectItem("4", "Enviado ao DERE"));//Adicionar Status no filtro situação
            status_item.remove(0);//Remove o status "todos" do filtro situação

            //FabricaDeConexoes.fecharConecxao();
        } catch (ErroSistema e) {
            F.setMsgErro("exportarSissaBean:init:" + e.toString());
        }
        Date dateIniXml = (Date) ContextoUtil.getParamFromSession("dataIniXml");//data inicial carregando da sessão após o download XML ser gerado
        Date dateFimXml = (Date) ContextoUtil.getParamFromSession("dataFimXml");//data final carregando da sessão após o download XML ser gerado
        String filtrosituacaoXml = (String) ContextoUtil.getParamFromSession("filtroSituacaoXml");//filtroSituação carregando ao contexto após o download XML ser gerado
        if (dateIniXml != null && dateFimXml != null && filtrosituacaoXml != null) { // verificar se foi utilizado a sessão após o download do xml for realizado
            dtIni = dateIniXml;
            dtFim = dateFimXml;
            filtroSituacao = filtrosituacaoXml;
            try {
                buscarApac();// realiza a busca novamente após o timed out da tela

            } catch (ErroSistema ex) {
                F.setMsgErro("exportarSissaBean:init:" + ex.toString());
            }
        }

    }

    public void buscarApac() throws ErroSistema {//busca formulários ao preencher e marcar campos na tela
//        getForms().clear();//limpa os formulários carregados na tela
        if (filtroSituacao.equals("4")) {
            rendereCheckBt = false;
        } else {
            rendereCheckBt = true;
        }
        String condicao = "WHERE (formulario.data between '" + F.dataStringBanco(dtIni) + "' AND '" + F.dataStringBanco(dtFim) + "') ";
        if (!(filtroSituacao.isEmpty() || filtroSituacao.equals("0"))) {
            condicao += " AND (formulario.status_id_status ='" + filtroSituacao + "' )";
        }
        List<tb_sissa> aux = new ArrayList<>();//Lista de formulários auxiliar
        setEntidades(getDao().buscarSissa(condicao));//Adiciona os formulários a uma lista com o comando SQL condição
        for (Formulario f : super.getEntidades()) {
            f.buscaProcedimentosForm();
//              if(verificaProcApac(f.getP1())){
            if (verificaProcAltaComplex(f.getP1())) {//Verifica se o procedimento do formulário é do tipo APAC
                tb_sissa fs = new tb_sissa();
                fs.adicionaFormulario(f);
//                System.out.println(fs.getPaciente().getNome());
                aux.add(fs);//Se o formulário for do tipo APAC então pode ser processado no sissa logo adicionamos para a lista
            }
        }
        Collections.sort(aux, new Comparator<tb_sissa>() {//comparador que ordena a lista de formulários por procedimento
            @Override
            public int compare(tb_sissa t, tb_sissa t1) {
                if (Integer.parseInt(t.getP1().getCodigo()) < Integer.parseInt(t1.getP1().getCodigo())) {
                    return -1;
                }
                if (Integer.parseInt(t.getP1().getCodigo()) > Integer.parseInt(t1.getP1().getCodigo())) {
                    return 1;
                }
                return 0;
            }
        });
        setForms(aux);//adiciona todos os formulários do tipo APAC na lista carregada na tela
    }
//botoes
// fim botoes
    //auxiliares

    @Override
    public void verificaPerfil(int perfil) {

        if (perfil == 3) {//3 administrador
            try {
                status_item = Status.item("id_status = 4 OR id_status = 3");
                status_item.remove(0);
            } catch (ErroSistema e) {

            }

        } else {
            try {

                status_item = Status.item("id_status = 4 OR id_status = 3");
                status_item.remove(0);
            } catch (ErroSistema ex) {
                Logger.getLogger(EmitidosBean.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

    public boolean isCheckBoxMarcado() {//retorna se está marcado ou não a caixa de seleção da tabela
        return checkBoxMarcado;
    }

    public void setCheckBoxMarcado(boolean checkBoxMarcado) {//  altera o status da marcação do datatable
        if (!getFormsSelecionados().isEmpty() && filtroSituacao.equals("3")) {
            rendereBtGerarXML = true;//mostra o botão gerar XML se estiver marcado algum formuário e se a busca for do tipo emitido
        } else {
            rendereBtGerarXML = false;
        }
        this.checkBoxMarcado = checkBoxMarcado;
    }

    public boolean isCheckBoxNaoMarcado() {//retorna se está marcado ou não a caixa de seleção da tabela
        return checkBoxNaoMarcado;
    }

    public void setCheckBoxNaoMarcado(boolean checkBoxNaoMarcado) {//  altera o status da marcação do datatable
        if (getFormsSelecionados().isEmpty() && !filtroSituacao.equals("3")) {
            rendereBtGerarXML = false;
        } else if (getFormsSelecionados().isEmpty() && filtroSituacao.equals("3")) {
            rendereBtGerarXML = false;
        } else if (!getFormsSelecionados().isEmpty() && filtroSituacao.equals("3")) {
            rendereBtGerarXML = true;
        }
        this.checkBoxNaoMarcado = checkBoxNaoMarcado;
    }

    public String mascaraCodProdedimento(String codProced) {//criar mascara da string procedimento principal
        String retur = "";
        for (int i = 0; i < codProced.length(); i++) {
            retur += codProced.charAt(i);
            if (i == 1 || i == 3 || i == 5) {
                retur += ".";
            }
            if (i == 8) {
                retur += "-";
            }
        }
        return retur;
    }

    public void abrirDlgConfirmaSissa() {
        DlgConfirmaSissaBean.listaStatica = getFormsSelecionados();
        DlgConfirmaSissaBean.dtIni = getDtIni();
        DlgConfirmaSissaBean.dtFim = getDtFim();
        DlgConfirmaSissaBean.filtroSituacao = getFiltroSituacao();

        F.abrirDlgPreenchimentoRapido("dlg/dlgConfirmaSissa", 1200, 500, true, false);
    }

    public boolean verificaProcAltaComplex(Procedimento_sus procedimento_sus) {//Verfica se o formulário é caracterizado como alta complexidade (incia com 03.04 )
        String p = "";
        for (int i = 0; i < 4; i++) {
            p += procedimento_sus.getCodigo().charAt(i);
        }
        if (p.equals("0304")) {
            return true;
        } else {
            return false;
        }

    }

    public boolean verificaProcAPAC(Procedimento_sus procedimento_sus) throws ErroSistema {//verifica se o procedimento é do tipo APAC
        Tb_registro_has_procedimento_sus registro = new Tb_registro_has_procedimento_sus();
        List<Tb_registro_has_procedimento_sus> registros = new Tb_registro_has_procedimento_susDAO().buscar("procedimento_sus_codigo=" + procedimento_sus.getCodigo());
        if (registros.size() > 0) {
            registro = registros.get(0);
            return true;
        } else {
            return false;
        }
    }

    //fim auxiliares
    public String mascaraProntuario(String prontuario) { // mascara para exibição do prontuário na tela
        String retur = "";

        for (int i = 0; i < prontuario.length(); i++) {
            if (i == 6) {
                retur += "-";
            }
            retur += prontuario.charAt(i);
        }
        return retur;
    }

    public String racaCorSissa(Formulario f) {//retorna o código da cor do paciente de acordo com que  o sissa escreve
        switch (f.getPaciente().getCor()) {
            case "A":
                return "04";
            case "B":
                return "01";
            case "I":
                return "05";
            case "M":
                return "03";
            case "P":
                return "02";
            default:
                return "";
        }
    }

    public List<tb_sissa> getFormsSelecionados() {
        return formsSelecionados;
    }

    public void setFormsSelecionados(List<tb_sissa> formsSelecionados) {
        this.formsSelecionados = formsSelecionados;
    }

    public List<tb_sissa> getForms() {
        return forms;
    }

    public void setForms(List<tb_sissa> forms) {
        this.forms = forms;
    }

    public String getTituloTabela() {
        tituloTabela = getForms().size() + " FORMULÁRIOS emitidos no período de " + F.dataString(dtIni) + " à " + F.dataString(dtFim);
        return tituloTabela;
    }

    public void setTituloTabela(String tituloTabela) {
        this.tituloTabela = tituloTabela;
    }

    public boolean isSelectBooleanCheckBox_periodo() {

        return selectBooleanCheckBox_periodo;
    }

    public void setSelectBooleanCheckBox_periodo(boolean selectBooleanCheckBox_periodo) {

        this.selectBooleanCheckBox_periodo = selectBooleanCheckBox_periodo;
    }

    public Date getDtIni() {
        return dtIni;
    }

    public boolean isRendereBtGerarXML() {
        if (filtroSituacao.equals("4")) {
            rendereBtGerarXML = false;
        }
        return rendereBtGerarXML;
    }

    public void setRendereBtGerarXML(boolean rendereBtGerarXML) {
        this.rendereBtGerarXML = rendereBtGerarXML;
    }

    public boolean isRendereCheckBt() {
        return rendereCheckBt;
    }

    public void setRendereCheckBt(boolean rendereCheckBt) {
        this.rendereCheckBt = rendereCheckBt;
    }

    public void setDtIni(Date dtIni) {
        selectBooleanCheckBox_periodo = false;
        if (dtIni.after(dtFim)) {
            dtFim = dtIni;
        }
        this.dtIni = dtIni;
    }

    public Date getDtFim() {

        return dtFim;
    }

    public void setDtFim(Date dtFim) {
        selectBooleanCheckBox_periodo = false;
        if (dtFim.before(dtIni)) {
            dtIni = dtFim;
        }
        this.dtFim = dtFim;
    }

    public ArrayList<SelectItem> getStatus_item() {
        return status_item;
    }

    public void setStatus_item(ArrayList<SelectItem> status_item) {
        this.status_item = status_item;
    }

    public String getFiltroSituacao() {
        if (filtroSituacao.equals("4")) {
            rendereCheckBt = false;
        } else {
            rendereCheckBt = true;
        }

        return filtroSituacao;
    }

    public void setFiltroSituacao(String filtroSituacao) {
        this.filtroSituacao = filtroSituacao;
    }

    public List<Formulario> getCodigosLaudos() {
        return codigosLaudos;
    }

    public void setCodigosLaudos(List<Formulario> codigosLaudos) {
        this.codigosLaudos = codigosLaudos;
    }

    public boolean isBtEnviarDere() {
        return btEnviarDere;
    }

    public void setBtEnviarDere(boolean btEnviarDere) {
        this.btEnviarDere = btEnviarDere;
    }

    public void retornoDlgConfirmaSissa(SelectEvent event) {
        List<tb_sissa> p = (List<tb_sissa>) event.getObject();
        if (p != null) {
            for (int i = 0; i < p.size(); i++) {
                if (!p.equals(new tb_sissa())) {
                    getForms().remove(p.get(i));
                    rendereBtGerarXML = false;
                }
            }

        }
    }
}
