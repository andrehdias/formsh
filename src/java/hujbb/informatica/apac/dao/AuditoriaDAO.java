package hujbb.informatica.apac.dao;

import hujbb.informatica.apac.entidades.Auditoria;
import hujbb.informatica.apac.entidades.Cbo;
import hujbb.informatica.apac.entidades.Entidade;
import hujbb.informatica.apac.entidades.Log_procedimento;
import hujbb.informatica.apac.entidades.Perfil;
import hujbb.informatica.apac.entidades.Setor;
import hujbb.informatica.apac.entidades.Usuario;
import hujbb.informatica.apac.util.F;
import hujbb.informatica.apac.util.FabricaDeConexoes;
import hujbb.informatica.apac.util.execao.ErroSistema;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AuditoriaDAO implements CrudDAO<Auditoria> {

    @Override
    public Auditoria salvar(Auditoria entidade) throws ErroSistema {
        Connection conexao = FabricaDeConexoes.getConexao();
        try {//, 

            String sql = "INSERT INTO auditoria( data, procedimento_realizado, usuario_id_usuario, tipo_entidade_id, id_entidade_alvo, log_procedimento_id, campo_antigo, campo_novo) VALUES (NOW(),?,?,?,?,?,?,?)";

            PreparedStatement ps = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            //  ps.setDate(1, F.sqlDate(entidade.getData()));
            // System.out.println("1:"+entidade.getProcedimento_realizado());
            ps.setString(1, entidade.getProcedimento_realizado());
            //   System.out.println("6:"+entidade.getUsuario().getAuditoriain());
            ps.setInt(2, entidade.getUsuario().getId_usuario());
            //   System.out.println("5:"+entidade.getTipo_entidade().getId());
            ps.setInt(3, entidade.getTipo_entidade().getId());
            //  System.out.println("7:"+entidade.getId_entidade_alvo());
            ps.setString(4, entidade.getId_entidade_alvo());
            //   System.out.println("4:"+entidade.getAuditoria_procedimento().getId());
            ps.setInt(5, entidade.getLog_procedimento().getId());
            //   System.out.println("2:"+entidade.getCampo_antigo());
            ps.setString(6, entidade.getCampo_antigo());
            //   System.out.println("3:"+entidade.getCampo_novo());
            ps.setString(7, entidade.getCampo_novo());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                entidade.setId(rs.getInt(1));
            }
        } catch (SQLException ex) {

            F.setMsgErro("auditoriaDAO 40! " + ex.toString());
            return null;
        }

        return entidade;
    }

    @Override
    public Auditoria atualizar(Auditoria entidade) throws ErroSistema {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Auditoria deletar(Auditoria entidade) throws ErroSistema {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Auditoria> buscar(String condicao) throws ErroSistema {
        condicao = F.tratarCondicaoSQL(condicao);
        try {
            Connection conexao = FabricaDeConexoes.getConexao();
            String sql = "SELECT\n"
                    + "     auditoria.id_auditoria AS auditoria_id_auditoria,\n"
                    + "     auditoria.data AS auditoria_data,\n"
                    + "     auditoria.procedimento_realizado AS auditoria_procedimento_realizado,\n"
                    + "     auditoria.id_entidade_alvo AS auditoria_id_entidade_alvo,\n"
                    + "     auditoria.campo_antigo AS auditoria_campo_antigo,\n"
                    + "     auditoria.campo_novo AS auditoria_campo_novo,\n"
                    + "     usuario.id_usuario AS usuario_id_usuario,\n"
                    + "     usuario.login AS usuario_login,\n"
                    + "     usuario.perfil AS usuario_perfil,\n"
                    + "     usuario.ativo AS usuario_ativo,\n"
                    + "     setor.id_setor AS setor_id_setor,\n"
                    + "     setor.nome AS setor_nome,\n"
                    + "     setor.sigla AS setor_sigla,\n"
                    + "     entidade.id AS entidade_id,\n"
                    + "     entidade.nome AS entidade_nome,\n"
                    + "     log_procedimento.id AS log_procedimento_id,\n"
                    + "     log_procedimento.descricao AS log_procedimento_descricao\n"
                    + "FROM\n"
                    + "     usuario usuario INNER JOIN auditoria auditoria ON usuario.id_usuario = auditoria.usuario_id_usuario\n"
                    + "     INNER JOIN setor setor ON usuario.setor_id_setor = setor.id_setor\n"
                    + "     INNER JOIN entidade entidade ON auditoria.tipo_entidade_id = entidade.id\n"
                    + "     INNER JOIN log_procedimento log_procedimento ON auditoria.log_procedimento_id = log_procedimento.id " + condicao;
           // System.out.println(sql);
            PreparedStatement ps = conexao.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Auditoria> auditorias = new ArrayList<>();
            while (rs.next()) {
                Auditoria auditoria = new Auditoria(
                        rs.getInt("auditoria_id_auditoria"),
                        rs.getDate("auditoria_data"),
                        rs.getString("auditoria_procedimento_realizado"),
                        rs.getString("auditoria_campo_antigo"),
                        rs.getString("auditoria_campo_novo"),
                        new Log_procedimento(
                                rs.getInt("log_procedimento_id"),
                                rs.getString("log_procedimento_descricao")
                        ),
                        new Entidade(
                                rs.getInt("entidade_id"),
                                rs.getString("entidade_nome")
                        ),
                        new Usuario(
                                rs.getInt("usuario_id_usuario"),
                                "",
                                rs.getString("usuario_login"),
                                "",
                                new Setor(
                                        rs.getInt("setor_id_setor"),
                                        rs.getString("setor_nome"),
                                        rs.getString("setor_sigla")
                                ),
                                new Perfil(),
                                1,
                                new Cbo(),
                                null
                        ),
                        rs.getString("auditoria_id_entidade_alvo")
                );
                auditorias.add(auditoria);
            }

            return auditorias;

        } catch (SQLException e) {
            throw new ErroSistema("Erro ao buscar dados da auditoria:"+e.toString());
        }
    }

    @Override
    public Auditoria buscaId(String id) throws ErroSistema {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
