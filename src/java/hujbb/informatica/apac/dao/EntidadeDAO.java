package hujbb.informatica.apac.dao;

import hujbb.informatica.apac.entidades.Entidade;
import hujbb.informatica.apac.util.F;
import hujbb.informatica.apac.util.FabricaDeConexoes;
import hujbb.informatica.apac.util.execao.ErroSistema;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EntidadeDAO implements CrudDAO<Entidade> {

    @Override
    public Entidade salvar(Entidade entidade) throws ErroSistema {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Entidade atualizar(Entidade entidade) throws ErroSistema {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Entidade deletar(Entidade entidade) throws ErroSistema {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Entidade> buscar(String condicao) throws ErroSistema {
         if (condicao.isEmpty()) {//todos os principais
            condicao = "WHERE id<> 2 OR id<> 3 OR id<> 4 OR id<> 6 OR id<> 8 OR id<> 10 OR id<> 12 OR id<> 14 OR id<> 15 OR id<> 16 OR id<> 18 OR id<> 21 OR id<> 22 OR id<> 23 OR id<> 19 OR id<> 24 OR id<> 25";
        } else {
            condicao = "WHERE" + condicao.replace("WHERE", "");
        }

        try {
            Connection conexao = FabricaDeConexoes.getConexao();
            String sql = "SELECT id, nome FROM entidade " + condicao + "  ORDER BY nome";
            PreparedStatement ps = conexao.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Entidade> entidades = new ArrayList<>();
            // System.out.println(sql);
            while (rs.next()) {
                Entidade entidade = new Entidade(
                        rs.getInt("id"),
                        rs.getString("nome")
                );
                entidades.add(entidade);
            }
            return entidades;

        } catch (SQLException e) {
            F.msgErro += F.hora() + "EntidadeDAO62: " + e.toString();
            return null;

        }

    }

    @Override
    public Entidade buscaId(String id) throws ErroSistema {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
