/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hujbb.informatica.apac.dao;

import hujbb.informatica.apac.entidades.Formulario_favorito;
import hujbb.informatica.apac.util.F;
import hujbb.informatica.apac.util.FabricaDeConexoes;
import hujbb.informatica.apac.util.execao.ErroSistema;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Formulario_favoritoDAO implements Serializable, CrudDAO<Formulario_favorito> {

    @Override
    public Formulario_favorito salvar(Formulario_favorito entidade) throws ErroSistema {
        Connection conexao = FabricaDeConexoes.getConexao();
        try {
            String sql = "INSERT INTO formulario_favorito(descricao, cod_procedimento1,cod_procedimento2,cod_procedimento3,cod_procedimento4,cod_procedimento5,cod_procedimento6, cid1,cid2,cid3,cid4,qtd1,qtd2,qtd3,qtd4,qtd5,qtd6, obs, solicitante_id_solicitante) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement ps = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, entidade.getDescricao());
            ps.setString(2, entidade.getCod_procedimento1());
            ps.setString(3, entidade.getCod_procedimento2());
            ps.setString(4, entidade.getCod_procedimento3());
            ps.setString(5, entidade.getCod_procedimento4());
            ps.setString(6, entidade.getCod_procedimento5());
            ps.setString(7, entidade.getCod_procedimento6());
            ps.setString(8, entidade.getCid1());
            ps.setString(9, entidade.getCid2());
            ps.setString(10, entidade.getCid3());
            ps.setString(11, entidade.getCid4());
            ps.setInt(12, entidade.getQtd1());
            ps.setInt(13, entidade.getQtd2());
            ps.setInt(14, entidade.getQtd3());
            ps.setInt(15, entidade.getQtd4());
            ps.setInt(16, entidade.getQtd5());
            ps.setInt(17, entidade.getQtd6());
            ps.setString(18, entidade.getObservacoes());
            ps.setInt(19, entidade.getId_solicitante());
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                entidade.setId(rs.getInt(1));
            }

        } catch (SQLException ex) {

            F.setMsgErro("formulario_favoritoDAO 55! " + ex.toString());

        }

        return entidade;
    }

    @Override
    public Formulario_favorito atualizar(Formulario_favorito entidade) throws ErroSistema {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Formulario_favorito deletar(Formulario_favorito entidade) throws ErroSistema {
        try {
            Connection conexao = FabricaDeConexoes.getConexao();
            PreparedStatement ps = conexao.prepareStatement("DELETE FROM formulario_favorito WHERE id_formulario_favorito = ?");
            ps.setInt(1, entidade.getId());
            ps.execute();

        } catch (SQLException ex) {

            F.setMsgErro("formulario_favoritoDAO72: " + ex);
            entidade = null;

        }

        return entidade;
    }

    @Override
    public List<Formulario_favorito> buscar(String condicao) throws ErroSistema {
        condicao = F.tratarCondicaoSQL(condicao);
        try {
            Connection conexao = FabricaDeConexoes.getConexao();
            String sql = "SELECT id_formulario_favorito,"
                    + " descricao,"
                    + " cod_procedimento1,"
                    + "cod_procedimento2,"
                    + "cod_procedimento3,"
                    + "cod_procedimento4,"
                    + "cod_procedimento5,"
                    + "cod_procedimento6, "
                    + "cid1,"
                    + "cid2,cid3,cid4,qtd1,qtd2,qtd3,qtd4,qtd5,qtd6, obs, solicitante_id_solicitante FROM formulario_favorito " + condicao;

            PreparedStatement ps = conexao.prepareStatement(sql);
            //System.out.println(sql);
            ResultSet rs = ps.executeQuery();
            List<Formulario_favorito> formulariosFavoritos = new ArrayList<>();

            while (rs.next()) {
                Formulario_favorito formularioFavorito = new Formulario_favorito();

                formularioFavorito.setId(rs.getInt("id_formulario_favorito"));
                formularioFavorito.setDescricao(rs.getString("descricao"));
                formularioFavorito.setCod_procedimento1(rs.getString("cod_procedimento1"));
                formularioFavorito.setCod_procedimento2(rs.getString("cod_procedimento2"));
                formularioFavorito.setCod_procedimento3(rs.getString("cod_procedimento3"));
                formularioFavorito.setCod_procedimento4(rs.getString("cod_procedimento4"));
                formularioFavorito.setCod_procedimento5(rs.getString("cod_procedimento5"));
                formularioFavorito.setCod_procedimento6(rs.getString("cod_procedimento6"));
                formularioFavorito.setCid1(rs.getString("cid1"));
                formularioFavorito.setCid2(rs.getString("cid2"));
                formularioFavorito.setCid3(rs.getString("cid3"));
                formularioFavorito.setCid4(rs.getString("cid4"));
                formularioFavorito.setQtd1(rs.getInt("qtd1"));
                formularioFavorito.setQtd2(rs.getInt("qtd2"));
                formularioFavorito.setQtd3(rs.getInt("qtd3"));
                formularioFavorito.setQtd4(rs.getInt("qtd4"));
                formularioFavorito.setQtd5(rs.getInt("qtd5"));
                formularioFavorito.setQtd6(rs.getInt("qtd6"));
                formularioFavorito.setObservacoes(rs.getString("obs"));
                formularioFavorito.setId_solicitante(rs.getInt("solicitante_id_solicitante"));

                formulariosFavoritos.add(formularioFavorito);
                
            }

            return formulariosFavoritos;

        } catch (SQLException e) {
            throw new ErroSistema("Erro ao buscar dados do formulários favoritos", e);
        }
    }

    public int contador(String condicao) throws ErroSistema {
        condicao = F.tratarCondicaoSQL(condicao);
        Connection conexao = FabricaDeConexoes.getConexao();
        int count = 0;
        try {

            String sql = "SELECT COUNT(*) FROM formulario_favorito " + condicao;
            PreparedStatement ps = conexao.prepareStatement(sql);
           // System.out.println(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                count = rs.getInt(1);
            }
            return count;
        } catch (SQLException e) {
            throw new ErroSistema("Erro ao contador formulario favorito!", e);
        }
    }

    @Override
    public Formulario_favorito buscaId(String id) throws ErroSistema {
        Formulario_favorito c = null;
        List<Formulario_favorito> l = buscar("WHERE id_formulario_favorito = " + id );
        if (l.size() > 0) {
            c = l.get(0);
        }
        return c;
    }

}
