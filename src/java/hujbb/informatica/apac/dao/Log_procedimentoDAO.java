package hujbb.informatica.apac.dao;



import hujbb.informatica.apac.entidades.Log_procedimento;
import hujbb.informatica.apac.util.F;
import hujbb.informatica.apac.util.FabricaDeConexoes;
import hujbb.informatica.apac.util.execao.ErroSistema;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Log_procedimentoDAO implements CrudDAO<Log_procedimento> {

    @Override
    public Log_procedimento salvar(Log_procedimento entidade) throws ErroSistema {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Log_procedimento atualizar(Log_procedimento entidade) throws ErroSistema {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Log_procedimento deletar(Log_procedimento entidade) throws ErroSistema {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Log_procedimento> buscar(String condicao) throws ErroSistema {
        condicao = F.tratarCondicaoSQL(condicao);

        try {
            Connection conexao = FabricaDeConexoes.getConexao();
            String sql = "SELECT id, descricao FROM log_procedimento " + condicao + "  ORDER BY descricao";
            PreparedStatement ps = conexao.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Log_procedimento> entidades = new ArrayList<>();
            // System.out.println(sql);
            while (rs.next()) {
                Log_procedimento entidade = new Log_procedimento(
                        rs.getInt("id"),
                        rs.getString("descricao")
                );
                entidades.add(entidade);
            }
            return entidades;

        } catch (SQLException e) {
            F.msgErro += F.hora() + "Log_procedimentoDAO52: " + e.toString();
            return null;

        }
    }

    @Override
    public Log_procedimento buscaId(String id) throws ErroSistema {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
