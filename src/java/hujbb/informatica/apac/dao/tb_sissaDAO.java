/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hujbb.informatica.apac.dao;

import hujbb.informatica.apac.entidades.Paciente;
import hujbb.informatica.apac.entidades.auxiliares.tb_sissa;
import hujbb.informatica.apac.util.F;
import hujbb.informatica.apac.util.FabricaDeConexoes;
import hujbb.informatica.apac.util.execao.ErroSistema;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author santana.andre
 */
public class tb_sissaDAO implements Serializable, CrudDAO<tb_sissa> {
    
    @Override
    public tb_sissa salvar(tb_sissa entidade) throws ErroSistema {
        Connection conexao = FabricaDeConexoes.getConexao();
        try {
            String sql = "INSERT INTO tb_sissa( arquivo, competencia, dth_criacao, formulario_id_formulario, codigo_laudo, seq_paciente, usuario) VALUES (?,?,?,?,?,?,?)";
            PreparedStatement ps = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, entidade.getArquivo());
            ps.setString(2, entidade.getCompetencia());
//            ps.setDate(3, F.sqlDate(entidade.getDth_criacao()));
            ps.setTimestamp(3, new Timestamp(entidade.getDth_criacao().getTime()));
            ps.setInt(4, entidade.getId_formulario());
            ps.setString(5, entidade.getCodLaudo());
            ps.setString(6, entidade.getSeqPaciente());
            ps.setInt(7, entidade.getUsuario());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
//            System.out.println(sql);
            if (rs.next()) {
                entidade.setId_sissa(rs.getInt(1));
            }

        } catch (SQLException ex) {

            F.setMsgErro("tb_sissaDAO 55! " + ex.toString());

        }

        return entidade;
    }
    
    @Override
    public tb_sissa atualizar(tb_sissa entidade) throws ErroSistema {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public tb_sissa deletar(tb_sissa entidade) throws ErroSistema {
        try {
            Connection conexao = FabricaDeConexoes.getConexao();
            PreparedStatement ps = conexao.prepareStatement("DELETE FROM tb_sissa WHERE formulario_id_formulario = ?");
            ps.setInt(1, entidade.getId_formulario());
            ps.execute();

        } catch (SQLException ex) {

            F.setMsgErro("tb_sissaDAO72: " + ex);
            entidade = null;

        }

        return entidade;

    }
    
    @Override
    public List<tb_sissa> buscar(String condicao) throws ErroSistema {
        try {
            Connection conexao = FabricaDeConexoes.getConexao();
            String sql = "SELECT id_sissa, arquivo, competencia, dth_criacao, formulario_id_formulario, codigo_laudo, seq_paciente, usuario FROM tb_sissa " + condicao;
            System.out.println(sql);
            PreparedStatement ps = conexao.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<tb_sissa> tb_sissas = new ArrayList<>();

            while (rs.next()) {
                tb_sissa tbsissa = new tb_sissa();

                tbsissa.setId_sissa(rs.getInt("id_sissa"));
                tbsissa.setArquivo(rs.getString("arquivo"));
                tbsissa.setCompetencia(rs.getString("competencia"));
                tbsissa.setDth_criacao(rs.getDate("dth_criacao"));
                tbsissa.setId_formulario(rs.getInt("formulario_id_formulario"));
                tbsissa.setCodLaudo(rs.getString("codigo_laudo"));
                tbsissa.setSeqPaciente(rs.getString("seq_paciente"));//sequencial do paciente
                tbsissa.setUsuario(rs.getInt("usuario"));//usuário logado
                

                tb_sissas.add(tbsissa);
            }

            return tb_sissas;

        } catch (SQLException e) {
            throw new ErroSistema("Erro ao buscar dados do paciente", e);
        }
        
        
    }
    
    public List<tb_sissa> buscarLotes(String condicao) throws ErroSistema {
        try {
            Connection conexao = FabricaDeConexoes.getConexao();
            String sql = "SELECT DISTINCT (arquivo),competencia,dth_criacao,usuario FROM tb_sissa";

            PreparedStatement ps = conexao.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<tb_sissa> tb_sissas = new ArrayList<>();

            while (rs.next()) {
                tb_sissa tbsissaLote = new tb_sissa();
                tbsissaLote.setArquivo(rs.getString("arquivo"));
                tbsissaLote.setCompetencia(rs.getString("competencia"));
                tbsissaLote.setDth_criacao(rs.getDate("dth_criacao"));
                tbsissaLote.setSeqPaciente(rs.getString("seq_paciente"));//sequencial do paciente
                tbsissaLote.setUsuario(rs.getInt("usuario"));

                tb_sissas.add(tbsissaLote);
            }

            return tb_sissas;

        } catch (SQLException e) {
            throw new ErroSistema("Erro ao buscar dados da tabela sissa lote", e);
        }
        
        
    }
    
    @Override
    public tb_sissa buscaId(String id) throws ErroSistema {
        tb_sissa c = null;
        List<tb_sissa> l = buscar("WHERE id_sissa= " + id);
        if (l.size() > 0) {
            c = l.get(0);
        }
        return c;
    }
    
    
    
}
