package hujbb.informatica.apac.entidades;

import hujbb.informatica.apac.util.F;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Auditoria implements Serializable {

    private Integer id_auditoria;
    private Date data;
    private String procedimento_realizado;
    private Usuario usuario;
    private Entidade tipo_entidade;
    private String id_entidade_alvo;
    private Log_procedimento log_procedimento;
    private String campo_antigo;
    private String campo_novo;

//variaveis extras
    private String dataString;
    private String afetado;

    public Auditoria() {
        this.id_auditoria = -1;
        this.data = new Date("01-01-1900");
        this.procedimento_realizado = "";
        this.campo_antigo = "";
        this.campo_novo = "";
        this.log_procedimento = new Log_procedimento();
        this.tipo_entidade = new Entidade();
        this.usuario = new Usuario();
        this.id_entidade_alvo = "";
        this.dataString = "-";
        this.afetado = "-";
    }

    public Auditoria(Integer id, Date data, String procedimento_realizado, String campo_antigo, String campo_novo, Log_procedimento log_procedimento, Entidade tipo_entidade, Usuario usuario, String id_entidade_alvo) {
        this.id_auditoria = id;
        this.data = data;
        this.procedimento_realizado = procedimento_realizado;
        this.campo_antigo = campo_antigo;
        this.campo_novo = campo_novo;
        this.log_procedimento = log_procedimento;
        this.tipo_entidade = tipo_entidade;
        this.usuario = usuario;
        this.id_entidade_alvo = id_entidade_alvo;
        this.dataString = "";
        this.afetado = "";
    }

    public Integer getId() {
        return id_auditoria;
    }

    public void setId(Integer id) {
        this.id_auditoria = id;
    }

    public String getProcedimento_realizado() {
        return procedimento_realizado;
    }

    public void setProcedimento_realizado(String procedimento_realizado) {
        this.procedimento_realizado = procedimento_realizado;
    }

    public Entidade getTipo_entidade() {
        return tipo_entidade;
    }

    public void setTipo_entidade(Entidade tipo_entidade) {
        this.tipo_entidade = tipo_entidade;
    }

    //retorna o numedo da entidade na tabela entidade no banco de dados
    public int nun_entidade_bd() {
        return 14;
    }

    public String getDataString() {
        if (id_auditoria == -1) {
            dataString = "";
        } else {
            dataString = F.dataString(data);
        }
        return dataString;
    }

    public void setDataString(String dataString) {
        this.dataString = dataString;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Entidade getTipo_entidade_id() {
        return tipo_entidade;
    }

    public void setTipo_entidade_id(Entidade tipo_entidade_id) {
        this.tipo_entidade = tipo_entidade_id;
    }

    public String getId_entidade_alvo() {
        return id_entidade_alvo;
    }

    public void setId_entidade_alvo(String id_entidade_alvo) {
        this.id_entidade_alvo = id_entidade_alvo;
    }

    public Log_procedimento getLog_procedimento() {
        return log_procedimento;
    }

    public void setLog_procedimento(Log_procedimento log_procedimento) {
        this.log_procedimento = log_procedimento;
    }

    public String getCampo_antigo() {
        return campo_antigo;
    }

    public void setCampo_antigo(String campo_antigo) {
        this.campo_antigo = campo_antigo;
    }

    public String getCampo_novo() {
        return campo_novo;
    }

    public void setCampo_novo(String campo_novo) {
        this.campo_novo = campo_novo;
    }

    public String getAfetado() {
        if (procedimento_realizado != null && !procedimento_realizado.contains("***")) {
            afetado = tipo_entidade.getNome().toUpperCase() + ": " + ((tipo_entidade.getId() == 7) ? Formulario.mascaraID(id_entidade_alvo) : id_entidade_alvo) + " " + procedimento_realizado;
        }else{
            afetado =  procedimento_realizado.replace("***", "");
        }
        return afetado;
    }

    public void setAfetado(String afetado) {

        this.afetado = afetado;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.id_auditoria);
        hash = 47 * hash + Objects.hashCode(this.data);
        hash = 47 * hash + Objects.hashCode(this.usuario);
        hash = 47 * hash + Objects.hashCode(this.tipo_entidade);
        hash = 47 * hash + Objects.hashCode(this.id_entidade_alvo);
        hash = 47 * hash + Objects.hashCode(this.log_procedimento);
        hash = 47 * hash + Objects.hashCode(this.campo_antigo);
        hash = 47 * hash + Objects.hashCode(this.campo_novo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Auditoria other = (Auditoria) obj;
        if (!Objects.equals(this.campo_antigo, other.campo_antigo)) {
            return false;
        }
        if (!Objects.equals(this.campo_novo, other.campo_novo)) {
            return false;
        }
        if (!Objects.equals(this.id_auditoria, other.id_auditoria)) {
            return false;
        }
        if (!Objects.equals(this.data, other.data)) {
            return false;
        }
        if (!Objects.equals(this.usuario, other.usuario)) {
            return false;
        }
        if (!Objects.equals(this.tipo_entidade, other.tipo_entidade)) {
            return false;
        }
        if (!Objects.equals(this.id_entidade_alvo, other.id_entidade_alvo)) {
            return false;
        }
        if (!Objects.equals(this.log_procedimento, other.log_procedimento)) {
            return false;
        }
        return true;
    }

}
