package hujbb.informatica.apac.entidades;

import hujbb.informatica.apac.dao.EntidadeDAO;
import hujbb.informatica.apac.util.FabricaDeConexoes;
import hujbb.informatica.apac.util.execao.ErroSistema;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.faces.model.SelectItem;

public class Entidade {

    private int id;
    private String nome;

    public Entidade() {
        id = -1;
        nome = "";
    }

    public Entidade(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    //retorna o numedo da entidade na tabela entidade no banco de dados
    public int nun_entidade_bd() {
        return 5;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    //itens
    public static ArrayList<SelectItem> item(String condicao) throws ErroSistema {
        ArrayList<SelectItem> item = new ArrayList<>();
        List<Entidade> m = new EntidadeDAO().buscar(condicao);
        //item.add(new SelectItem("0", "Selecione"));
        for (int i = 0; i < m.size(); i++) {
            item.add(new SelectItem(m.get(i).getId() + "", m.get(i).getNome()));
        }
        FabricaDeConexoes.fecharConecxao();
        return item;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.id);
        hash = 17 * hash + Objects.hashCode(this.nome);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Entidade other = (Entidade) obj;
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

}
