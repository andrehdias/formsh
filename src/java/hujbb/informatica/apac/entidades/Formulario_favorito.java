package hujbb.informatica.apac.entidades;

import java.io.Serializable;

public class Formulario_favorito implements Serializable {
    //variaveis do banco
    private Integer id;
    private String descricao;
    private String cod_procedimento1;
    private String cod_procedimento2;
    private String cod_procedimento3;
    private String cod_procedimento4;
    private String cod_procedimento5;
    private String cod_procedimento6;
    private String cid1;
    private String cid2;
    private String cid3;
    private String cid4;
    private Integer qtd1;
    private Integer qtd2;
    private Integer qtd3;
    private Integer qtd4;
    private Integer qtd5;
    private Integer qtd6;
    private String observacoes;
    private Integer id_solicitante;

    public Formulario_favorito(Integer id,String descricao,String cod_procedimento1,String cod_procedimento2,
                               String cod_procedimento3,String cod_procedimento4,String cod_procedimento5,
                               String cod_procedimento6,String cid1,String cid2,String cid3,String cid4,
                               Integer qtd1,Integer qtd2,Integer qtd3,Integer qtd4,Integer qtd5,Integer qtd6,
                               String observacoes,Integer id_solicitante){
        this.id=id;
        this.descricao=descricao;
        this.cod_procedimento1=cod_procedimento1;
        this.cod_procedimento2=cod_procedimento2;
        this.cod_procedimento3=cod_procedimento3;
        this.cod_procedimento4=cod_procedimento4;
        this.cod_procedimento5=cod_procedimento5;
        this.cod_procedimento6=cod_procedimento6;
        this.cid1=cid1;
        this.cid2=cid2;
        this.cid3=cid3;
        this.cid4=cid4;
        this.qtd1=qtd1;
        this.qtd2=qtd2;
        this.qtd3=qtd3;
        this.qtd4=qtd4;
        this.qtd5=qtd5;
        this.qtd6=qtd6;
        
        this.observacoes=observacoes;
        this.id_solicitante=id_solicitante;
    }

    public Formulario_favorito(){
        this.id=0;
        this.descricao="";
        this.cod_procedimento1="";
        this.cod_procedimento2="";
        this.cod_procedimento3="";
        this.cod_procedimento4="";
        this.cod_procedimento5="";
        this.cod_procedimento6="";
        this.cid1="";
        this.cid2="";
        this.cid3="";
        this.cid4="";
        this.qtd1 =0;
        this.qtd2 =0;
        this.qtd3 =0;
        this.qtd4 =0;
        this.qtd5 =0;
        this.qtd6 =0;
        this.observacoes="";
        this.id_solicitante=0;
    }

    public boolean isVazio(){
        if(this.cod_procedimento1!=null && !this.cod_procedimento1.trim().isEmpty())return false;
        if(this.cod_procedimento2!=null && !this.cod_procedimento2.trim().isEmpty())return false;
        if(this.cod_procedimento3!=null && !this.cod_procedimento3.trim().isEmpty())return false;
        if(this.cod_procedimento4!=null && !this.cod_procedimento4.trim().isEmpty())return false;
        if(this.cod_procedimento5!=null && !this.cod_procedimento5.trim().isEmpty())return false;
        if(this.cod_procedimento6!=null && !this.cod_procedimento6.trim().isEmpty())return false;
        if(this.cid1!=null && !this.cid1.trim().isEmpty())return false;
        if(this.cid2!=null && !this.cid2.trim().isEmpty())return false;
        if(this.cid3!=null && !this.cid3.trim().isEmpty())return false;
        if(this.cid4!=null && !this.cid4.trim().isEmpty())return false;
        if(this.observacoes!=null && !this.observacoes.trim().isEmpty())return false;

        return true;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCod_procedimento1() {
        return cod_procedimento1;
    }

    public void setCod_procedimento1(String cod_procedimento1) {
        this.cod_procedimento1 = cod_procedimento1;
    }

    public String getCod_procedimento2() {
        return cod_procedimento2;
    }

    public void setCod_procedimento2(String cod_procedimento2) {
        this.cod_procedimento2 = cod_procedimento2;
    }

    public String getCod_procedimento3() {
        return cod_procedimento3;
    }

    public void setCod_procedimento3(String cod_procedimento3) {
        this.cod_procedimento3 = cod_procedimento3;
    }

    public String getCod_procedimento4() {
        return cod_procedimento4;
    }

    public void setCod_procedimento4(String cod_procedimento4) {
        this.cod_procedimento4 = cod_procedimento4;
    }

    public String getCod_procedimento5() {
        return cod_procedimento5;
    }

    public void setCod_procedimento5(String cod_procedimento5) {
        this.cod_procedimento5 = cod_procedimento5;
    }
    public String getCod_procedimento6() {
        return cod_procedimento6;
    }

    public void setCod_procedimento6(String cod_procedimento6) {
        this.cod_procedimento6= cod_procedimento6;
    }

    public String getCid1() {
        return cid1;
    }

    public void setCid1(String cid1) {
        this.cid1 = cid1;
    }

    public String getCid2() {
        return cid2;
    }

    public void setCid2(String cid2) {
        this.cid2 = cid2;
    }

    public String getCid3() {
        return cid3;
    }

    public void setCid3(String cid3) {
        this.cid3 = cid3;
    }

    public String getCid4() {
        return cid4;
    }

    public void setCid4(String cid4) {
        this.cid4 = cid4;
    }

    public Integer getQtd1() {
        return qtd1;
    }

    public void setQtd1(Integer qtd1) {
        this.qtd1 = qtd1;
    }

    public Integer getQtd2() {
        return qtd2;
    }

    public void setQtd2(Integer qtd2) {
        this.qtd2 = qtd2;
    }

    public Integer getQtd3() {
        return qtd3;
    }

    public void setQtd3(Integer qtd3) {
        this.qtd3 = qtd3;
    }

    public Integer getQtd4() {
        return qtd4;
    }

    public void setQtd4(Integer qtd4) {
        this.qtd4 = qtd4;
    }

    public Integer getQtd5() {
        return qtd5;
    }

    public void setQtd5(Integer qtd5) {
        this.qtd5 = qtd5;
    }

    public Integer getQtd6() {
        return qtd6;
    }

    public void setQtd6(Integer qtd6) {
        this.qtd6 = qtd6;
    }
    
    



    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public Integer getId_solicitante() {
        return id_solicitante;
    }

    public void setId_solicitante(Integer id_solicitante) {
        this.id_solicitante = id_solicitante;
    }

}
