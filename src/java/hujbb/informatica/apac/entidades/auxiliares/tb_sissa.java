/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hujbb.informatica.apac.entidades.auxiliares;

import hujbb.informatica.apac.entidades.Autorizacao;
import hujbb.informatica.apac.entidades.Estabelecimento_de_saude;
import hujbb.informatica.apac.entidades.Formulario;
import hujbb.informatica.apac.entidades.Formulario_f2;
import hujbb.informatica.apac.entidades.Motivo_Cancelamento;
import hujbb.informatica.apac.entidades.Paciente;
import hujbb.informatica.apac.entidades.Proc_justificativa;
import hujbb.informatica.apac.entidades.Solicitante;
import hujbb.informatica.apac.entidades.Status;
import hujbb.informatica.apac.util.execao.ErroSistema;
import java.util.Date;

/**
 *
 * @author santana.andre
 */
public class tb_sissa extends Formulario{
    private int id_sissa;
    private String arquivo;
    private String competencia;
    private Date dth_criacao;
    private String codLaudo = "";
    private String seqPaciente="";
    private Integer usuario=0;
    
    
    public tb_sissa(
            Integer id_formulario,
            Date data,
            Date data_criacao,
            String autenticacao, 
            Paciente paciente, 
            Solicitante solicitante, 
            Estabelecimento_de_saude estabelecimento_de_saude_solicitante, 
            Estabelecimento_de_saude estabelecimento_de_saude_executante, 
            Proc_justificativa proc_justificativa, 
            Status status, 
            Autorizacao autorizacao,
            Motivo_Cancelamento motivo_cancelamento,
            String setor,
            String codLaudo,
            String seqPaciente
    ) {
        super(id_formulario, data, data_criacao, autenticacao, paciente, solicitante, estabelecimento_de_saude_solicitante, estabelecimento_de_saude_executante, proc_justificativa, status, autorizacao, motivo_cancelamento, setor);
        this.codLaudo = codLaudo;
        this.seqPaciente=seqPaciente;
    }
    
    public tb_sissa(){
        super();
    }

    public int getId_sissa() {
        return id_sissa;
    }

    public void setId_sissa(int id_sissa) {
        this.id_sissa = id_sissa;
    }

    public String getArquivo() {
        return arquivo;
    }

    public void setArquivo(String arquivo) {
        this.arquivo = arquivo;
    }

    public String getCompetencia() {
        return competencia;
    }

    public void setCompetencia(String competencia) {
        this.competencia = competencia;
    }

    public Date getDth_criacao() {
        return dth_criacao;
    }

    public void setDth_criacao(Date dth_criacao) {
        this.dth_criacao = dth_criacao;
    }

    public String getCodLaudo() {
        return codLaudo;
    }

    public void setCodLaudo(String codLaudo) {
        this.codLaudo = codLaudo;
    }

    public String getSeqPaciente() {
        return seqPaciente;
    }

    public void setSeqPaciente(String seqPaciente) {
        this.seqPaciente = seqPaciente;
    }

    public Integer getUsuario() {
        return usuario;
    }

    public void setUsuario(Integer usuario) {
        this.usuario = usuario;
    }
    
    public void adicionaFormulario(Formulario f) throws ErroSistema{
        super.setId_formulario(f.getId_formulario());
        super.setData(f.getData());
        super.setData_criacao(f.getData_criacao());
        super.setAutenticacao(f.getAutenticacao());
        super.setPaciente(f.getPaciente());
        super.setSolicitante(f.getSolicitante());
        super.setEstabelecimento_de_saude_solicitante(f.getEstabelecimento_de_saude_solicitante());
        super.setEstabelecimento_de_saude_executante(f.getEstabelecimento_de_saude_executante());
        super.setProc_justificativa(f.getProc_justificativa());
        super.setStatus(f.getStatus());
        super.setAutorizacao(f.getAutorizacao());
        super.setMotivo_cancelamento(f.getMotivo_cancelamento());
        super.setSetor(f.getSetor());
        super.setPag2(f.getPag2());
        super.setP1(f.getP1());
    }
    
    
        
}
