package hujbb.informatica.apac.entidades.imprimir;

public class Pdf_relatorioSissa {

    private String nSolicitacao;
    private String nome;
    private String cartaoSus;
    private String codigoProcedimento;
    private String periodo;
    private String prontuario;
    private String municipioResid;
    private String codCid;
    private String dataeHoraAtual;
    private String cnes;
    private String nomeEstabelecimento;
    private String totalGeralSol;

    public Pdf_relatorioSissa(String nSolicitacao, String nome, String cartaoSus, String codigoProcedimento,
            String periodo,String prontuario,String municipioResid,String codCid, String dataeHoraAtual, 
            String cnes, String nomeEstabelecimento, String totalGeralSol) {
        this.nSolicitacao = nSolicitacao;
        this.nome = nome;
        this.cartaoSus = cartaoSus;
        this.codigoProcedimento = codigoProcedimento;
        this.periodo = periodo;
        this.prontuario=prontuario;
        this.municipioResid=municipioResid;
        this.codCid=codCid;
        this.dataeHoraAtual=dataeHoraAtual;
        this.cnes=cnes;
        this.nomeEstabelecimento=nomeEstabelecimento;
        this.totalGeralSol=totalGeralSol;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCartaoSus() {
        return cartaoSus;
    }

    public void setCartaoSus(String cartaoSus) {
        this.cartaoSus = cartaoSus;
    }

    public String getCodigoProcedimento() {
        return codigoProcedimento;
    }

    public void setCodigoProcedimento(String codigoProcedimento) {
        this.codigoProcedimento = codigoProcedimento;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getnSolicitacao() {
        return nSolicitacao;
    }

    public void setnSolicitacao(String nSolicitacao) {
        this.nSolicitacao = nSolicitacao;
    }

    public String getProntuario() {
        return prontuario;
    }

    public void setProntuario(String prontuario) {
        this.prontuario = prontuario;
    }

    public String getMunicipioResid() {
        return municipioResid;
    }

    public void setMunicipioResid(String municipioResid) {
        this.municipioResid = municipioResid;
    }

    public String getCodCid() {
        return codCid;
    }

    public void setCodCid(String codCid) {
        this.codCid = codCid;
    }

    public String getDataeHoraAtual() {
        return dataeHoraAtual;
    }

    public void setDataeHoraAtual(String dataeHoraAtual) {
        this.dataeHoraAtual = dataeHoraAtual;
    }

    public String getCnes() {
        return cnes;
    }

    public void setCnes(String cnes) {
        this.cnes = cnes;
    }

    public String getNomeEstabelecimento() {
        return nomeEstabelecimento;
    }

    public void setNomeEstabelecimento(String nomeEstabelecimento) {
        this.nomeEstabelecimento = nomeEstabelecimento;
    }

    public String getTotalGeralSol() {
        return totalGeralSol;
    }

    public void setTotalGeralSol(String totalGeralSol) {
        this.totalGeralSol = totalGeralSol;
    }
        
    
}
