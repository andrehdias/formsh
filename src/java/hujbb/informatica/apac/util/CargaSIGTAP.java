package hujbb.informatica.apac.util;

import hujbb.informatica.apac.dao.CboDAO;
import hujbb.informatica.apac.dao.CidDAO;
import hujbb.informatica.apac.dao.Cid_has_procedimento_susDAO;
import hujbb.informatica.apac.dao.CompetenciaDAO;
import hujbb.informatica.apac.dao.Forma_organizacaoDAO;
import hujbb.informatica.apac.dao.Grupo_procedimentoDAO;
import hujbb.informatica.apac.dao.Procedimento_susDAO;
import hujbb.informatica.apac.dao.Procedimento_sus_has_cboDAO;
import hujbb.informatica.apac.dao.Sub_grupo_procedimentoDAO;
import hujbb.informatica.apac.dao.Tb_descricaoDAO;
import hujbb.informatica.apac.dao.Tb_financiamentoDAO;
import hujbb.informatica.apac.dao.Tb_modalidadeDAO;
import hujbb.informatica.apac.dao.Tb_registroDAO;
import hujbb.informatica.apac.dao.Tb_registro_has_procedimento_susDAO;
import hujbb.informatica.apac.entidades.Cbo;
import hujbb.informatica.apac.entidades.Cid_has_procedimento_sus;
import hujbb.informatica.apac.entidades.Cid;
import hujbb.informatica.apac.entidades.Competencia;
import hujbb.informatica.apac.entidades.Forma_organizacao;
import hujbb.informatica.apac.entidades.Grupo_procedimento;
import hujbb.informatica.apac.entidades.Procedimento_sus;
import hujbb.informatica.apac.entidades.Procedimento_sus_has_cbo;
import hujbb.informatica.apac.entidades.Sub_grupo_procedimento;
import hujbb.informatica.apac.entidades.Tb_descricao;
import hujbb.informatica.apac.entidades.Tb_financiamento;
import hujbb.informatica.apac.entidades.Tb_modalidade;
import hujbb.informatica.apac.entidades.Tb_registro;
import hujbb.informatica.apac.entidades.Tb_registro_has_procedimento_sus;
import hujbb.informatica.apac.util.execao.ErroSistema;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class CargaSIGTAP implements Serializable {

    private String caminhaZip;
    private List<String> coluna;
    private List<Integer> tamanho;
    private List<Integer> inicio;
    private List<Integer> fim;
    private List<String> linhasTxt;
    List<String> arrayLinhas;
    private FileWriter logCarga;
    private PrintWriter gravarLogCarga;
    private String caminhoLogCarga = "";
    private String sql = "";

    public CargaSIGTAP(String caminhoZip) throws ErroSistema, IOException {
        this.caminhaZip = caminhoZip;
        linhasTxt = new ArrayList<>();
        coluna = new ArrayList<>();
        tamanho = new ArrayList<>();
        inicio = new ArrayList<>();
        fim = new ArrayList<>();
        zerarLayout();
        caminhoLogCarga = FacesContext.getCurrentInstance().getExternalContext().getRealPath("")
                + F.getArqLogCaminho();
        logCarga = new FileWriter(caminhoLogCarga + "logCarga" + new SimpleDateFormat("ddMMyyyy").format(Calendar.getInstance().getTime()) + ".txt", true);
        gravarLogCarga = new PrintWriter(logCarga);
        gravarLogCarga.println("");
        gravarLogCarga.flush();
        if (!verificarCompetencia()) {
            if (compararLayout()) {
                gravarLogCarga.println("Não houve mudança no layout das tabelas");
                gravarLogCarga.flush();
                if (verificaCarga()) {
                    F.mensagem("", "CARGA PROCESSADA COM SUCESSO!", FacesMessage.SEVERITY_INFO);
                    gravarLogCarga.println("");
                    gravarLogCarga.println("Conteúdo gravado no banco de dados com sucesso.");
                    gravarLogCarga.println("");
                    gravarLogCarga.println("CARGA PROCESSADA COM SUCESSO, PROCESSAMENTO FINALIZADO!");
                    gravarLogCarga.println("FINALIZADO EM:"
                            + new SimpleDateFormat("dd/MM/yyyy, HH:mm").format(Calendar.getInstance().getTime()));
                    gravarLogCarga.println("_________________________________________________________________________________");
                    gravarLogCarga.println("");
                    gravarLogCarga.flush();
                    gravarLogCarga.close();
                } else {
                    F.mensagem("ERRO AO PROCESSAR CARGA!", "Não foi possível gravar no banco de dados.", FacesMessage.SEVERITY_ERROR);
                    gravarLogCarga.println("");
                    gravarLogCarga.println("CARGA NÃO PROCESSADA, PROCESSAMENTO INTERROMPIDO!");
                    gravarLogCarga.println("FINALIZADO EM:"
                            + new SimpleDateFormat("dd/MM/yyyy, HH:mm").format(Calendar.getInstance().getTime()));
                    gravarLogCarga.println("_________________________________________________________________________________");
                    gravarLogCarga.println("");
                    gravarLogCarga.flush();
                    gravarLogCarga.close();
                }
            } else {
                F.mensagem("ERRO AO PROCESSAR CARGA!", "Houve mudança de layout em uma ou mais tabelas.", FacesMessage.SEVERITY_ERROR);
                gravarLogCarga.println("");
                gravarLogCarga.println("CARGA NÃO PROCESSADA, PROCESSAMENTO INTERROMPIDO!");
                gravarLogCarga.println("FINALIZADO EM:"
                        + new SimpleDateFormat("dd/MM/yyyy, HH:mm").format(Calendar.getInstance().getTime()));
                gravarLogCarga.println("_________________________________________________________________________________");
                gravarLogCarga.println("");
                gravarLogCarga.flush();
                gravarLogCarga.close();
            }
        } else {
            F.mensagem("ERRO AO PROCESSAR CARGA!", "Carga da competência " + lerCompetencia() + " já processada anteriormente", FacesMessage.SEVERITY_ERROR);
            gravarLogCarga.println("");
            gravarLogCarga.println("Carga da competência " + lerCompetencia() + " já processada anteriormente");
            gravarLogCarga.println("");
            gravarLogCarga.println("CARGA NÃO PROCESSADA, PROCESSAMENTO INTERROMPIDO!");
            gravarLogCarga.println("FINALIZADO EM:"
                    + new SimpleDateFormat("dd/MM/yyyy, HH:mm").format(Calendar.getInstance().getTime()));
            gravarLogCarga.println("_________________________________________________________________________________");
            gravarLogCarga.println("");
            gravarLogCarga.flush();
            gravarLogCarga.close();
        }
        FabricaDeConexoes.fecharConecxao();
    }

    private int lerCompetencia() {
        String linhaStr;
        String linha1 = "";
        int size = 0;
        int competencia = 0;
        //linhasTxt.clear();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(F.descompactarUmArquivoNoZip(caminhaZip, "tb_grupo.txt")), "ISO-8859-1"));
            linhaStr = br.readLine();
            competencia = Integer.parseInt(linhaStr.substring(102, 108));
            br.close();
        } catch (IOException ioe) {
            F.setMsgErro(ioe.toString());
        } catch (NullPointerException ioe) {
            F.setMsgErro("lerCompetencia" + ioe.toString());
        }
        return (competencia);
    }

    private boolean verificarCompetencia() throws ErroSistema {
        Competencia competencia = new Competencia();
        CompetenciaDAO competenciaDao = new CompetenciaDAO();
        List<Competencia> competencias = new CompetenciaDAO().buscar("WHERE 1=1");
        for (int i = 0; i < competencias.size(); i++) {
            competencia = competencias.get(i);
            if (competencia.getCompetencia() == lerCompetencia()) {
                i = competencias.size();
                return true;
            }
        }
        return false;
    }

    private boolean cargaGrupo_procedimento() throws ErroSistema {
        F.intAux = 10;
        boolean salvarCompetencia = false;
        pegarosdados("tb_grupo");
        Grupo_procedimento conteudo = new Grupo_procedimento();
        Grupo_procedimentoDAO dao = new Grupo_procedimentoDAO();
        sql = "INSERT INTO grupo_procedimento(id, nome, dt_competencia) VALUES\n";
        for (String linha : linhasTxt) {
            for (int i = 0; i < coluna.size(); i++) {//2
                switch (i) {
                    case 0: {//CO_GRUPO
                        conteudo.setId(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 1: {//NO_GRUPO
                        conteudo.setNome(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 2: {//DT_COMPETENCIA
                        try {
                            conteudo.setDt_competencia(Integer.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                            //salva a competencia
                            if (!salvarCompetencia) {
                                new CompetenciaDAO().salvar(new Competencia(conteudo.getDt_competencia()));
                                salvarCompetencia = true;
                            }
                        } catch (NumberFormatException e) {
                            F.setMsgErro(e.toString() + ":cargaSigtap 112");
                        }
                        sql += " ('" + conteudo.getId() + "','" + conteudo.getNome() + "','" + conteudo.getDt_competencia() + "'),\n";
                        conteudo = new Grupo_procedimento();
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }//fim for 2
        }//for 1
        sql = sql.substring(0, sql.length() - 2) + ";\n\n\n";
        if (F.executSql(sql)) {
            return true;
        } else {
            gravarLogCarga.println("Não foi possível salvar no banco de dados(tabela grupo_procedimento) os dados do arquivo tb_grupo.txt");
            gravarLogCarga.flush();
            return false;
        }
    }

    private boolean cargaSub_grupo_procedimento() throws ErroSistema {
        F.intAux = 15;
        pegarosdados("tb_sub_grupo");
        Sub_grupo_procedimento conteudo = new Sub_grupo_procedimento();
        Sub_grupo_procedimentoDAO dao = new Sub_grupo_procedimentoDAO();
        sql = "INSERT INTO sub_grupo_procedimento(id, grupo_procedimento_id, nome, dt_competencia) VALUES\n";
        for (String linha : linhasTxt) {
            for (int i = 0; i < coluna.size(); i++) {//2
                switch (i) {
                    case 0: {//CO_GRUPO
                        conteudo.getGrupo_Procedimento().setId(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 1: {//CO_SUB_GRUPO(cod gp + cod sub gp)
                        conteudo.setId(
                                conteudo.getGrupo_Procedimento().getId() //cod gp
                                + linha.substring(inicio.get(i) - 1, fim.get(i)).trim() //cod sub gp
                        );
                        break;
                    }
                    case 2: {//NO_SUB_GRUPO
                        conteudo.setNome(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 3: {//DT_COMPETENCIA
                        try {
                            conteudo.setDt_Competencia(Integer.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        } catch (NumberFormatException e) {
                            F.setMsgErro(e.toString() + ":cargaSigtap 102");
                        }
                        sql += " ('" + conteudo.getId() + "','" + conteudo.getGrupo_Procedimento().getId() + "','" + conteudo.getNome() + "','" + conteudo.getDt_Competencia() + "'),\n";

                        conteudo = new Sub_grupo_procedimento();
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }//fim for 2
        }//for 1
        sql = sql.substring(0, sql.length() - 2) + ";\n\n\n";
        if (F.executSql(sql)) {
            return true;
        } else {
            gravarLogCarga.println("Não foi possível salvar no banco de dados(tabela sub_grupo_procedimento) os dados do arquivo tb_sub_grupo.txt");
            gravarLogCarga.flush();
            return false;
        }
    }

    private boolean cargaForma_organizacao() throws ErroSistema {
        F.intAux = 25;
        pegarosdados("tb_forma_organizacao");
        Forma_organizacao conteudo = new Forma_organizacao();
        Forma_organizacaoDAO dao = new Forma_organizacaoDAO();
        sql = "INSERT INTO forma_organizacao(id, sub_grupo_procedimento_id, nome, dt_competencia)VALUES\n";
        for (String linha : linhasTxt) {
            for (int i = 0; i < coluna.size(); i++) {//2
                switch (i) {
                    case 0: {//CO_GRUPO
                        conteudo.getSub_grupo_procedimento().getGrupo_Procedimento().setId(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 1: {//CO_SUB_GRUPO (cod gp + cod sub gp)
                        conteudo.getSub_grupo_procedimento().setId(
                                conteudo.getSub_grupo_procedimento().getGrupo_Procedimento().getId() // cod gp
                                + linha.substring(inicio.get(i) - 1, fim.get(i)).trim() // cod sub gp
                        );
                        break;
                    }
                    case 2: {//CO_FORMA_ORGANIZACAO (cod gp + cd sub gp + cod forma)
                        conteudo.setId(
                                conteudo.getSub_grupo_procedimento().getId() // cod gp + cod sub gp
                                + linha.substring(inicio.get(i) - 1, fim.get(i)).trim() // cod forma
                        );
                        break;
                    }
                    case 3: {//NO_FORMA_ORGANIZACAO
                        conteudo.setNome(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 4: {//DT_COMPETENCIA
                        try {
                            conteudo.setDt_competencia(Integer.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        } catch (NumberFormatException e) {
                            F.setMsgErro(e.toString() + ":cargaSigtap 148");
                        }
                        sql += " ('" + conteudo.getId() + "','" + conteudo.getSub_grupo_procedimento().getId() + "','" + conteudo.getNome() + "','" + conteudo.getDt_competencia() + "'),\n";
                        conteudo = new Forma_organizacao();
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }//fim for 2
        }//for 1
        sql = sql.substring(0, sql.length() - 2) + ";\n\n\n";
        if (F.executSql(sql)) {
            return true;
        } else {
            gravarLogCarga.println("Não foi possível salvar no banco de dados(tabela forma_organizacao) os dados do arquivo tb_forma_organizacao.txt");
            gravarLogCarga.flush();
            return false;
        }
    }

    private boolean cargaCbo() throws ErroSistema {
        F.intAux = 30;
        pegarosdados("tb_ocupacao");
        Cbo conteudo = new Cbo();
        CboDAO dao = new CboDAO();
        for (String linha : linhasTxt) {
            for (int i = 0; i < coluna.size(); i++) {//2
                switch (i) {
                    case 0: {//CO_OCUPACAO
                        conteudo.setCod(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 1: {//NO_OCUPACAO
                        conteudo.setNome(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        if (dao.salvar(conteudo) == null) {
                            gravarLogCarga.println("Não foi possível salvar no banco de dados(tabela tb_ocupacao) os dados do arquivo tb_ocupacao.txt");
                            gravarLogCarga.flush();
                            return false;
                        }
                        conteudo = new Cbo();
                        break;
                    }
                }
            }//fim for 2
        }
        return true;
    }

    private boolean cargaCid() throws ErroSistema {
        F.intAux = 33;
        pegarosdados("tb_cid");
        Cid conteudo = new Cid();
        CidDAO dao = new CidDAO();
        for (String linha : linhasTxt) {
            for (int i = 0; i < coluna.size(); i++) {//2
                switch (i) {
                    case 0: {//CO_CID
                        conteudo.setCid(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 1: {//NO_CID
                        conteudo.setNome(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 3: {//TP_SEXO
                        conteudo.getTp_sexo().setId(linha.substring(inicio.get(i) - 1, fim.get(i)).trim().toLowerCase());
                        if (dao.salvar(conteudo) == null) {
                            gravarLogCarga.println("Não foi possível salvar no banco de dados(tabela cid) os dados do arquivo tb_cid.txt");
                            gravarLogCarga.flush();
                            return false;
                        }
                        conteudo = new Cid();
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }//fim for 2
        }
        return true;
    }

    private boolean cargaProced_financiamento() throws ErroSistema {
        F.intAux = 46;
        pegarosdados("tb_financiamento");
        Tb_financiamento conteudo = new Tb_financiamento();
        Tb_financiamentoDAO dao = new Tb_financiamentoDAO();
        sql = "INSERT INTO proced_financiamento(id, nome, dt_competencia) VALUES\n";
        for (String linha : linhasTxt) {
            for (int i = 0; i < coluna.size(); i++) {//2
                switch (i) {
                    case 0: {//CO_FINANCIAMENTO
                        conteudo.setId(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 1: {//NO_FINANCIAMENTO
                        conteudo.setNome(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 2: {//DT_COMPETENCIA
                        try {
                            conteudo.setDt_competencia(Integer.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        } catch (NumberFormatException e) {
                            F.setMsgErro(e.toString() + ":cargaSigtap 260");
                        }
                        sql += " ('" + conteudo.getId() + "','" + conteudo.getNome() + "','" + conteudo.getDt_competencia() + "'),\n";

                        conteudo = new Tb_financiamento();
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }//fim for 2
        }//for 1
        sql = sql.substring(0, sql.length() - 2) + ";\n\n\n";
        if (F.executSql(sql)) {
            return true;
        } else {
            gravarLogCarga.println("Não foi possível salvar no banco de dados(tabela proced_financiamento) os dados do arquivo tb_financiamento.txt");
            gravarLogCarga.flush();
            return false;
        }
    }

    private boolean cargaModalidade() throws ErroSistema {
        F.intAux = 38;
        pegarosdados("tb_modalidade");
        Tb_modalidade conteudo = new Tb_modalidade();
        Tb_modalidadeDAO dao = new Tb_modalidadeDAO();
        sql = "INSERT INTO tb_modalidade(id, modalidade, dt_competencia) VALUES\n";
        for (String linha : linhasTxt) {
            for (int i = 0; i < coluna.size(); i++) {//2
                switch (i) {
                    case 0: {//CO_MODALIDADE
                        conteudo.setId(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 1: {//NO_MODALIDADE
                        conteudo.setNome(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 2: {//DT_COMPETENCIA
                        try {
                            conteudo.setDt_competencia(Integer.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        } catch (NumberFormatException e) {
                            F.setMsgErro(e.toString() + ":cargaSigtap 300");
                        }
                        sql += " ('" + conteudo.getId() + "','" + conteudo.getNome() + "','" + conteudo.getDt_competencia() + "'),\n";
                        conteudo = new Tb_modalidade();
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }//fim for 2
        }//for 1
        sql = sql.substring(0, sql.length() - 2) + ";\n\n\n";
        if (F.executSql(sql)) {
            return true;
        } else {
            gravarLogCarga.println("Não foi possível salvar no banco de dados(tabela tb_modalidade) os dados do arquivo tb_modalidade.txt");
            gravarLogCarga.flush();
            return false;
        }
    }

    private boolean cargaProcedimentos() throws ErroSistema {
        F.intAux = 51;
        pegarosdados("tb_procedimento");
        Procedimento_sus conteudo = new Procedimento_sus();
        Procedimento_susDAO dao = new Procedimento_susDAO();
        sql = "INSERT INTO procedimento_sus(codigo, nome, proced_tp_compexidade_id, tp_sexo_id, qtd_max, qtd_pontos, qtd, idade_min, idade_max, valor_hospitalar, valor_ambulatorial, valor_profissonal, proced_financiamento_id, tb_modalidade_id, dt_competencia, proced_financiamento_dt, tb_modalidade_dt)VALUES\n";
        for (String linha : linhasTxt) {
            for (int i = 0; i < coluna.size(); i++) {//2
                switch (i) {
                    case 0: {//CO_PROCEDIMENTO
                        conteudo.setCodigo(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 1: {//NO_PROCEDIMENTO
                        conteudo.setNome(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 2: {//TP_COMPLEXIDADE
                        conteudo.getProced_tp_compexidade().setId(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 3: {//TP_SEXO
                        conteudo.getTp_sexo().setId(linha.substring(inicio.get(i) - 1, fim.get(i)).trim().toLowerCase());
                        break;
                    }
                    case 4: {//QT_MAXIMA_EXECUCAO
                        conteudo.setQtd_max(F.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        break;
                    }
                    case 6: {//QT_PONTOS
                        conteudo.setQtd_pontos(F.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        break;
                    }
                    case 7: {//VL_IDADE_MINIMA
                        conteudo.setIdade_min(F.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        break;
                    }
                    case 8: {//VL_IDADE_MAXIMA
                        conteudo.setIdade_max(F.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        break;
                    }
                    case 9: {//VL_SH
                        conteudo.setValor_hospitalar(F.parseFloat(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        break;
                    }
                    case 10: {//VL_SA
                        conteudo.setValor_ambulatorial(F.parseFloat(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        break;
                    }
                    case 11: {//VL_SP
                        conteudo.setValor_proficional(F.parseFloat(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        break;
                    }
                    case 12: {//CO_FINANCIAMENTO
                        conteudo.getProced_financiamento().setId(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 15: {//DT_COMPETENCIA
                        try {
                            conteudo.setDt_competencia(Integer.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                            conteudo.getProced_financiamento().setDt_competencia(Integer.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        } catch (NumberFormatException e) {
                            F.setMsgErro(e.toString() + ":cargaSigtap 384");
                        }
                        sql += " ('"
                                + conteudo.getCodigo() + "','"
                                + conteudo.getNome() + "','"
                                + conteudo.getProced_tp_compexidade().getId() + "','"
                                + conteudo.getTp_sexo().getId() + "','"
                                + conteudo.getQtd_max() + "','"
                                + conteudo.getQtd_pontos() + "','"
                                + conteudo.getQtd() + "','"
                                + conteudo.getIdade_min() + "','"
                                + conteudo.getIdade_max() + "','"
                                + conteudo.getValor_hospitalar() + "','"
                                + conteudo.getValor_ambulatorial() + "','"
                                + conteudo.getValor_proficional() + "','"
                                + conteudo.getProced_financiamento().getId() + "','"
                                + conteudo.getTb_modalidade().getId() + "','"
                                + conteudo.getDt_competencia()+ "','"
                                + conteudo.getProced_financiamento().getDt_competencia()+ "','"
                                + 0//tb_modalidade dt_competencia vai ''
                                + "'),\n";
                        conteudo = new Procedimento_sus();
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }//fim for 2
        }//for 1
        sql = sql.substring(0, sql.length() - 2) + ";\n\n\n";
        if (F.executSql(sql)) {
            return true;
        } else {
            gravarLogCarga.println("Não foi possível salvar no banco de dados(tabela procedimento_sus) os dados do arquivo tb_procedimento.txt");
            gravarLogCarga.flush();
            return false;
        }
    }

    private boolean cargaRl_procedimento_modalidade() throws ErroSistema {
        F.intAux = 63;
        pegarosdados("rl_procedimento_modalidade");
        String codModalidade = "";
        String codProcedimento = "";
        int dt_competencia = -1;
        Procedimento_susDAO dao = new Procedimento_susDAO();
        for (String linha : linhasTxt) {
            for (int i = 0; i < coluna.size(); i++) {//2
                switch (i) {
                    case 0: {//CO_PROCEDIMENTO
                        codProcedimento = linha.substring(inicio.get(i) - 1, fim.get(i)).trim();
                        break;
                    }
                    case 1: {//CO_MODALIDADE
                        codModalidade = linha.substring(inicio.get(i) - 1, fim.get(i)).trim();
                        break;
                    }
                    case 2: {//DT_COMPETENCIA
                        try {
                            dt_competencia = Integer.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        } catch (NumberFormatException e) {
                            F.setMsgErro(e.toString() + ":cargaSigtap 593");
                        }
                        if (!dao.atualizarCarga(codProcedimento, dt_competencia, codModalidade)) {
                            gravarLogCarga.println("Não foi possível salvar no banco de dados(Update da tabela procedimento_sus) os dados do arquivo tb_cid.txt");
                            gravarLogCarga.flush();
                            return false;
                        }
                        codProcedimento = "";
                        codModalidade = "";
                        dt_competencia = -1;
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }//fim for 2
        }
        return true;
    }

    private boolean cargaProcedimento_sus_has_cbo() throws ErroSistema, ErroSistema {
        F.intAux = 69;
        pegarosdados("rl_procedimento_ocupacao");
        Procedimento_sus_has_cbo conteudo = new Procedimento_sus_has_cbo();
        Procedimento_sus_has_cboDAO dao = new Procedimento_sus_has_cboDAO();
        sql = "INSERT INTO procedimento_sus_has_cbo(procedimento_sus_codigo, cbo_id, dt_competencia) VALUES\n";
        int cont = 0;
        for (String linha : linhasTxt) {
            for (int i = 0; i < coluna.size(); i++) {//2
                switch (i) {
                    case 0: {//CO_PROCEDIMENTO
                        conteudo.setProcedimento_sus_codigo(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 1: {//CO_OCUPACAO
                        conteudo.setCbo_id(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 2: {//DT_COMPETENCIA
                        try {
                            conteudo.setDt_competencia(Integer.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        } catch (NumberFormatException e) {
                            F.setMsgErro(e.toString() + ":cargaSigtap 419");
                        }
                        if (cont > 15000) {
                            sql = sql.substring(0, sql.length() - 2) + ";\n\n\n";
                            F.executSql(sql);
                            cont = 0;
                            sql = "INSERT INTO procedimento_sus_has_cbo(procedimento_sus_codigo, cbo_id, dt_competencia) VALUES\n";
                        }
                        sql += " ('"
                                + conteudo.getProcedimento_sus_codigo() + "','"
                                + conteudo.getCbo_id() + "','"
                                + conteudo.getDt_competencia()
                                + "'),\n";
                        cont++;
                        conteudo = new Procedimento_sus_has_cbo();
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }//fim for 2
        }//for 1
        sql = sql.substring(0, sql.length() - 2) + ";\n\n\n";
        if (F.executSql(sql)) {
            return true;
        } else {
            gravarLogCarga.println("Não foi possível salvar no banco de dados(tabela procedimento_sus_has_cbo) os dados do arquivo rl_procedimento_modalidade.txt");
            gravarLogCarga.flush();
            return false;
        }
    }

    private boolean cargaCid_has_procedimento_sus() throws ErroSistema {
        F.intAux = 80;
        pegarosdados("rl_procedimento_cid");
        Cid_has_procedimento_sus conteudo = new Cid_has_procedimento_sus();
        Cid_has_procedimento_susDAO dao = new Cid_has_procedimento_susDAO();
        sql = "INSERT INTO cid_has_procedimento_sus(cid_cid, procedimento_sus_codigo, cid_principal, dt_competencia) VALUES\n";
        int cont = 0;
        for (String linha : linhasTxt) {
            for (int i = 0; i < coluna.size(); i++) {//2
                switch (i) {
                    case 0: {//CO_PROCEDIMENTO
                        conteudo.setCod_procedimento(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 1: {//CO_CID
                        conteudo.setCod_cid(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 2: {//ST_PRINCIPAL
                        if (linha.substring(inicio.get(i) - 1, fim.get(i)).trim().equals("S")) {
                            conteudo.setPrincipal(true);
                        } else {
                            conteudo.setPrincipal(false);
                        }
                        break;
                    }
                    case 3: {//DT_COMPETENCIA
                        try {
                            conteudo.setDt_competencia(Integer.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        } catch (NumberFormatException e) {
                            F.setMsgErro(e.toString() + ":cargaSigtap 465");
                        }
                        if (cont > 15000) {
                            sql = sql.substring(0, sql.length() - 2) + ";\n\n\n";
                            F.executSql(sql);
                            cont = 0;
                            sql = "INSERT INTO cid_has_procedimento_sus(cid_cid, procedimento_sus_codigo, cid_principal, dt_competencia) VALUES\n";
                        }
                        int isPrincipalCid = (conteudo.isPrincipal()) ? 1 : 0;//tranformando booleano para inteiro
                        sql += " ('"
                                + conteudo.getCod_cid() + "','"
                                + conteudo.getCod_procedimento() + "',"
                                + isPrincipalCid + ",'"
                                + conteudo.getDt_competencia()
                                + "'),\n";
                        conteudo = new Cid_has_procedimento_sus();
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }//fim for 2
        }//for1
        sql = sql.substring(0, sql.length() - 2) + ";\n\n\n";
        if (F.executSql(sql)) {
            return true;
        } else {
            gravarLogCarga.println("Não foi possível salvar no banco de dados(tabela cid_has_procedimento_sus) os dados do arquivo rl_procedimento_cid.txt");
            gravarLogCarga.flush();
            return false;
        }
    }

    private boolean cargaTb_registro() throws ErroSistema {
        F.intAux = 86;
        pegarosdados("tb_registro");
        Tb_registro conteudo = new Tb_registro();
        Tb_registroDAO dao = new Tb_registroDAO();
        sql = "INSERT INTO tb_registro(id, nome, dt_competecia) VALUES\n";
        for (String linha : linhasTxt) {
            for (int i = 0; i < coluna.size(); i++) {//2
                switch (i) {
                    case 0: {//CO_REGISTRO
                        conteudo.setId(F.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        break;
                    }
                    case 1: {//NO_REGISTRO
                        conteudo.setNome(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 2: {//DT_COMPETENCIA
                        try {
                            conteudo.setDt_competencia(Integer.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        } catch (NumberFormatException e) {
                            F.setMsgErro(e.toString() + ":cargaSigtap 419");
                        }
                        sql += " ('"
                                + conteudo.getId() + "','"
                                + conteudo.getNome() + "','"
                                + conteudo.getDt_competencia()
                                + "'),\n";
                        conteudo = new Tb_registro();
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }//fim for 2
        }//for 1
        sql = sql.substring(0, sql.length() - 2) + ";\n\n\n";
        if (F.executSql(sql)) {
            return true;
        } else {
            gravarLogCarga.println("Não foi possível salvar no banco de dados(tabela tb_registro) os dados do arquivo tb_registro.txt");
            gravarLogCarga.flush();
            return false;
        }
    }

    private boolean cargaTb_registro_has_procedimento_sus() throws ErroSistema {
        F.intAux = 99;
        pegarosdados("rl_procedimento_registro");
        Tb_registro_has_procedimento_sus conteudo = new Tb_registro_has_procedimento_sus();
        Tb_registro_has_procedimento_susDAO dao = new Tb_registro_has_procedimento_susDAO();
        sql = "INSERT INTO tb_registro_has_procedimento_sus(tb_registro_id, procedimento_sus_codigo, dt_competencia, procedimento_sus_dt) VALUES\n";
        for (String linha : linhasTxt) {
            for (int i = 0; i < coluna.size(); i++) {//2
                switch (i) {
                    case 0: {//CO_PROCEDIMENTO
                        conteudo.setProcedimento_sus_codigo(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 1: {//CO_REGISTRO
                        conteudo.setTb_registro_id(F.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        break;
                    }
                    case 2: {//DT_COMPETENCIA
                        try {
                            conteudo.setDt_competencia(Integer.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        } catch (NumberFormatException e) {
                            F.setMsgErro(e.toString() + ":cargaSigtap 541");
                        }
                        sql += " ('"
                                + conteudo.getTb_registro_id() + "','"
                                + conteudo.getProcedimento_sus_codigo() + "','"
                                + conteudo.getDt_competencia()+ "','"
                                + conteudo.getDt_competencia()
                                + "'),\n";
                        conteudo = new Tb_registro_has_procedimento_sus();
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }//fim for 2
        }//for 1
        sql = sql.substring(0, sql.length() - 2) + ";\n\n\n";
        if (F.executSql(sql)) {
            return true;
        } else {
            gravarLogCarga.println("Não foi possível salvar no banco de dados(tabela tb_registro_has_procedimento_sus) os dados do arquivo rl_procedimento_registro.txt");
            gravarLogCarga.flush();
            return false;
        }
    }

    private boolean cargaTb_descricao() throws ErroSistema {
        F.intAux = 100;
        pegarosdados("tb_descricao");
        Tb_descricao conteudo = new Tb_descricao();
        Tb_descricaoDAO dao = new Tb_descricaoDAO();
        sql = "INSERT INTO tb_descricao(procedimento_sus_codigo, descricao, dt_competencia) VALUES\n";
        int cont = 0;
        for (String linha : linhasTxt) {
            for (int i = 0; i < coluna.size(); i++) {//2
                switch (i) {
                    case 0: {//CO_PROCEDIMENTO
                        conteudo.setCod_procedimento(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 1: {//DS_PROCEDIMENTO
                        conteudo.setDescricao(linha.substring(inicio.get(i) - 1, fim.get(i)).trim());
                        break;
                    }
                    case 2: {//DT_COMPETENCIA
                        try {
                            conteudo.setDt_competencia(Integer.parseInt(linha.substring(inicio.get(i) - 1, fim.get(i)).trim()));
                        } catch (NumberFormatException e) {
                            F.setMsgErro(e.toString() + ":cargaSigtap 580");
                        }
                        if (cont > 5000) {
                            sql = sql.substring(0, sql.length() - 2) + ";\n\n\n";
                            F.executSql(sql);
                            cont = 0;
                            sql = "INSERT INTO tb_descricao(procedimento_sus_codigo, descricao, dt_competencia) VALUES\n";
                        }
                        sql += " ('"
                                + conteudo.getCod_procedimento() + "','"
                                + conteudo.getDescricao().replace("'", "") + "','"
                                + conteudo.getDt_competencia()
                                + "'),\n";
                        conteudo = new Tb_descricao();
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }//fim for 2
        }//for 1
        sql = sql.substring(0, sql.length() - 2) + ";\n\n\n";
        if (F.executSql(sql)) {
            return true;
        } else {
            gravarLogCarga.println("Não foi possível salvar no banco de dados(tabela tb_descricao) os dados do arquivo tb_descricao.txt");
            gravarLogCarga.flush();
            return false;
        }
    }

    private void pegarosdados(String nomeArqTxt) {
        setLayout(nomeArqTxt);
        lertxt(F.descompactarUmArquivoNoZip(caminhaZip, nomeArqTxt + ".txt"));
    }

    private void setLayout(String layuotTxt) {
        zerarLayout();
        lertxt(F.descompactarUmArquivoNoZip(caminhaZip, layuotTxt + "_layout.txt"));
        String linhaAtual = "";
        //ler cada linha do vetor(LinhasTxt)
        for (int i = 1; i < linhasTxt.size(); i++) {//1
            linhaAtual = linhasTxt.get(i);
            //ler cada caracter da linha atual
            String dados[] = linhaAtual.split(",");
            coluna.add(dados[0]);
            tamanho.add(Integer.parseInt(dados[1]));
            inicio.add(Integer.parseInt(dados[2]));
            fim.add(Integer.parseInt(dados[3]));
        }//fim for 1
    }

    private void zerarLayout() {
        coluna.clear();
        tamanho.clear();
        inicio.clear();
        fim.clear();
    }

    public void lertxt(File file) {
        String linha;
        linhasTxt.clear();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "ISO-8859-1"));
            linha = br.readLine();
            while (linha != null) {
                linhasTxt.add(F.tratarStringBanco(linha));
                linha = br.readLine();
            }
            br.close();
        } catch (IOException ioe) {
            F.setMsgErro(ioe.toString());
        } catch (NullPointerException ioe) {
            F.setMsgErro(ioe.toString());
            F.mensagem(file.getName(), "Não encontrado no arquivo zip!", FacesMessage.SEVERITY_ERROR);
        }
    }

    private boolean compararLayout() {
        boolean retorno = true;
        List<String> arquivos = new ArrayList<>();
        arquivos.add("tb_grupo_layout.txt");
        arquivos.add("tb_sub_grupo_layout.txt");
        arquivos.add("tb_forma_organizacao_layout.txt");
        arquivos.add("tb_ocupacao_layout.txt");
        arquivos.add("tb_cid_layout.txt");
        arquivos.add("tb_financiamento_layout.txt");
        arquivos.add("tb_procedimento_layout.txt");
        arquivos.add("rl_procedimento_modalidade_layout.txt");
        arquivos.add("rl_procedimento_ocupacao_layout.txt");
        arquivos.add("rl_procedimento_cid_layout.txt");
        arquivos.add("tb_registro_layout.txt");
        arquivos.add("rl_procedimento_registro_layout.txt");
        arquivos.add("tb_descricao_layout.txt");
        for (String arquivo : arquivos) {
            String caminhoArqLayoutPadrao = FacesContext.getCurrentInstance().getExternalContext().getRealPath("")
                    + F.getLayoutPadraoCaminho() + arquivo;
            File ArqLayoutPadrao = new File(caminhoArqLayoutPadrao);
            File ArqLayoutUp = F.descompactarUmArquivoNoZip(caminhaZip, arquivo);
            List<String> conteudoLayoutPadrao = new ArrayList<>();
            List<String> conteudoLayoutUp = new ArrayList<>();
            lertxt(ArqLayoutPadrao);
            conteudoLayoutPadrao = linhasTxt;
            linhasTxt = new ArrayList<>();
            lertxt(ArqLayoutUp);
            conteudoLayoutUp = linhasTxt;
            linhasTxt = new ArrayList<>();
            String caracterUp = "";
            String caracterPadrao = "";
            for (String linha : conteudoLayoutUp) {
                caracterUp += linha.replace(" ", "").toUpperCase();
            }
            for (String linha : conteudoLayoutPadrao) {
                caracterPadrao += linha.replace(" ", "").toUpperCase();
            }
            if (!caracterPadrao.equals(caracterUp)) {
                gravarLogCarga.println("Arquivo " + arquivo + " modificado.");
                gravarLogCarga.flush();
                retorno = false;
            }
        }
        return (retorno);
    }

    private boolean verificaCarga() throws ErroSistema {
        boolean retorno = true;
        FabricaDeConexoes.commitFalse();
        if (!cargaGrupo_procedimento()) {
            retorno = false;
        }
        if (!cargaSub_grupo_procedimento()) {
            retorno = false;
        }
        if (!cargaForma_organizacao()) {
            retorno = false;
        }
//        if (!cargaCid()) {
//            retorno = false;
//        }
        if (!cargaModalidade()) {
            retorno = false;
        }
        if (!cargaProced_financiamento()) {
            retorno = false;
        }
        if (!cargaProcedimentos()) {
            retorno = false;
        }
        if (!cargaRl_procedimento_modalidade()) {
            retorno = false;
        }
        if (!cargaProcedimento_sus_has_cbo()) {
            retorno = false;
        }
        if (!cargaCid_has_procedimento_sus()) {
            retorno = false;
        }
        if (!cargaTb_registro()) {
            retorno = false;
        }
        if (!cargaTb_registro_has_procedimento_sus()) {
            retorno = false;
        }
        if (!cargaTb_descricao()) {
            retorno = false;
        }
        if (retorno == true) {
            FabricaDeConexoes.commit();
        }
        return (retorno);
    }

}
