
package hujbb.informatica.apac.util;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.Flash;


public class ContextoUtil {
    
    public static final FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }

    public static final ExternalContext getExternalContext() {
        return getFacesContext().getExternalContext();
    }

    public static final Object addParamInRequest(String key, String value) {
        return getExternalContext().getRequestParameterMap().put(key, value);
    }

    public static final Object getParamFromRequest(String key) {
        return getExternalContext().getRequestParameterMap().get(key);
    }

    public static final void addParamInSession(String key, Object value) {
        getExternalContext().getSessionMap().put(key, value);
    }

    public static final Object getParamFromSession(String key) {
        Object r = getExternalContext().getSessionMap().get(key);
        getExternalContext().getSessionMap().remove(key);
        return r;
    }

    public static final void removeParamFromSession(String key) {
        getExternalContext().getSessionMap().remove(key);
    }

    public static final Flash getFlash() {
        return getExternalContext().getFlash();
    }

    public static void addParamInFlash(String key, Object value) {
        getFlash().put(key, value);
    }

    public static Object getParamFromFlash(String key) {
        return getFlash().get(key);
    }


/*
    private FacesContext facesContext;
    private static final String IDENT_REQUEST_PARAM = "@";

    public ContextoUtil() {
    }


    public FacesContext getFacesContext() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (context == null) {
            throw new ContextNotActiveException("FacesContext is not active");
        } else {
            return context;
        }
    }

    public ExternalContext getExternalContext() {
        return this.facesContext.getExternalContext();
    }

    public String getRealPath() {
        return this.getExternalContext().getRealPath("");
    }

    public HttpSession getSessao() {
        return (HttpSession)this.getExternalContext().getSession(true);
    }

    public HttpServletResponse getResponse() {
        return (HttpServletResponse)this.getExternalContext().getResponse();
    }

    public HttpServletRequest getRequest() {
        return (HttpServletRequest)this.getExternalContext().getRequest();
    }

    public void setParamSession(String key, Object object) {
        this.getSessao().setAttribute(key, object);
    }

    public Object getParamSession(String key) {
        return this.getSessao().getAttribute(key);
    }

    public void removeAttribute(String key) {
        this.getSessao().removeAttribute(key);
    }

    public Object getParamRequest(String nome) {
        return nome.startsWith("@") ? this.getParamSession(nome) : this.getParamSession("@" + nome);
    }

    public Object popParamRequest(String nome, Object defaultValue) {
        Object r = this.getParamRequest(nome);
        this.removeParamRequest(nome);
        return r == null ? defaultValue : r;
    }

    public void removeParamRequest(String nome) {
        if (nome.startsWith("@")) {
            this.removeAttribute(nome);
        } else {
            this.removeAttribute("@" + nome);
        }

    }

    public void setParamRequest(String nome, Object valor) {
        if (nome.startsWith("@")) {
            this.setParamSession(nome, valor);
        } else {
            this.setParamSession("@" + nome, valor);
        }

    }

    public void clearRequestParams() {
        Enumeration attrs = this.getSessao().getAttributeNames();

        while(attrs.hasMoreElements()) {
            String key = (String)attrs.nextElement();
            if (key.startsWith("@")) {
                this.removeParamRequest(key);
            }
        }

    }*/
    
}
